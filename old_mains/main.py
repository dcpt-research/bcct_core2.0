# -*- coding: utf-8 -*-
"""
Created on Mon Aug 22 08:15:45 2022
@authors: Morten Sahlertz & Jacob Kjærager
"""

import os
import cv2 as cv
import numpy as np
import pandas as pd
from PIL import Image
from typing import Dict, List, Tuple, Union
import SimpleITK as sitk
from pathlib import Path
from matplotlib import patches
import matplotlib.pyplot as plt
from plotly.offline import plot
import plotly.graph_objects as go
from sklearn.decomposition import PCA
from dataimport import import_data, import_tempdata
from cvzone.SelfiSegmentationModule import SelfiSegmentation


def get_breast_information(side: str, df: pd.DataFrame) -> List[Union[dict, pd.DataFrame, pd.DataFrame]]:
        right_group = df[df.columns[df.columns.str.contains(side)]]
        df_nipple = df[right_group.columns[right_group.columns.str.contains("nipple")]]
        nipple = {"x": df_nipple["{} nipple x".format(side)],
                  "y": df_nipple["{} nipple y".format(side)]}
        breast_x_coordinates = df[right_group.columns[right_group.columns.str.contains("x")]].drop(columns=['{} nipple x'.format(side)])
        breast_y_coordinates = df[right_group.columns[right_group.columns.str.contains("y")]].drop(columns=['{} nipple y'.format(side)])
        return [nipple, breast_x_coordinates, breast_y_coordinates]

def remove_background(color_for_cropping: tuple, image: any, threshold: float = 0.4) -> np.array:

    segmentor = SelfiSegmentation()
    image_no_background = segmentor.removeBG(image,
                                             imgBg=color_for_cropping,
                                             threshold=threshold)
    im_bw = cv.cvtColor(image_no_background,
                        cv.COLOR_BGR2GRAY)  # greyscale the image for single channel images: greylevel = (R+B+G)/3
    grey_level_of_color_for_cropping = sum(color_for_cropping)/len(color_for_cropping)
    im_bw[im_bw != grey_level_of_color_for_cropping] = 1
    im_bw[im_bw == grey_level_of_color_for_cropping] = 0
    holefill_filter = sitk.BinaryFillholeImageFilter()
    holefill_filter.SetForegroundValue(1)
    sitk_np = sitk.GetImageFromArray(np.ascontiguousarray(im_bw))
    filled_mask = holefill_filter.Execute(sitk_np)
    numpy_matrix_of_image = sitk.GetArrayFromImage(filled_mask)

    return (image * numpy_matrix_of_image[..., None])

class Image_handler:
    def __init__(self,
                 path: Path,
                 datafile_filename: str,
                 color_for_cropping: tuple):

        self.path = path
        self.image = cv.imread(str(path), cv.IMREAD_UNCHANGED)
        self.width = self.image.shape[1]
        self.height = self.image.shape[0]
        self.obtain_breast_coordinates(datafile_filename)
        self.color_for_cropping = color_for_cropping


    def plot_image(self, add_breast_contours: bool = False) -> None:
        plt.imshow(cv.cvtColor(self.image, cv.COLOR_BGR2RGB))
        if add_breast_contours:
            plt.scatter(self.left_breast_x_coordinates,
                        self.left_breast_y_coordinates,
                        s=25, c='blue', marker='o')
            plt.scatter(self.right_breast_x_coordinates,
                        self.right_breast_y_coordinates,
                        s=25, c='red', marker='o')
            plt.scatter(self.left_nipple['x'],
                        self.left_nipple['y'],
                        s=25, c='yellow', marker='o')
            plt.scatter(self.right_nipple['x'],
                        self.right_nipple['y'],
                        s=25, c='green', marker='o')
        plt.show()


    def crop_image(self) -> None:
        np_rgb_image = remove_background(self.color_for_cropping, self.image)
        summed_rgb_df = pd.DataFrame(np_rgb_image.sum(axis=2))
        cropped_df = summed_rgb_df.loc[summed_rgb_df[summed_rgb_df.sum(axis=1) != 0].index, summed_rgb_df.T[summed_rgb_df.T.sum(axis=1) != 0].index]
        x_cropped = np_rgb_image[cropped_df.index.values, :, :]
        fully_cropped = x_cropped[:, cropped_df.columns.values, :]
        # Generate variables
        plt.imshow(cv.cvtColor(fully_cropped, cv.COLOR_BGR2RGB))
        plt.show()
        left_crop_column = cropped_df.columns[0]
        high_crop_row = cropped_df.index[0]

        self.left_breast_x_coordinates = self.left_breast_x_coordinates - left_crop_column
        self.left_breast_y_coordinates = self.left_breast_y_coordinates - high_crop_row

        self.right_breast_x_coordinates = self.right_breast_x_coordinates - left_crop_column
        self.right_breast_y_coordinates = self.right_breast_y_coordinates - high_crop_row

        self.left_nipple["x"] = self.left_nipple["x"] - left_crop_column
        self.left_nipple["y"] = self.left_nipple["y"] - high_crop_row

        self.right_nipple["x"] = self.right_nipple["x"] - left_crop_column
        self.right_nipple["y"] = self.right_nipple["y"] - high_crop_row      

        self.width = cropped_df.columns[-1] - left_crop_column
        self.height = cropped_df.index[-1] - high_crop_row
        self.image = fully_cropped

    def resize_image(self, pixel_width: int, pixel_height: int) -> None:

        width_percentage = pixel_width / self.width
        height_percentage = pixel_height / self.height

        self.left_breast_x_coordinates = self.left_breast_x_coordinates * width_percentage
        self.right_breast_x_coordinates = self.right_breast_x_coordinates * width_percentage
        self.left_breast_y_coordinates = self.left_breast_y_coordinates * height_percentage
        self.right_breast_y_coordinates = self.right_breast_y_coordinates * height_percentage

        self.left_nipple['x'] = self.left_nipple['x'] * width_percentage
        self.right_nipple['x'] = self.right_nipple['x'] * width_percentage
        self.left_nipple['y'] = self.left_nipple['y'] * height_percentage
        self.right_nipple['y'] = self.right_nipple['y'] * height_percentage
        self.width = pixel_width
        self.height = pixel_height

        self.image = cv.resize(self.image, (pixel_width, pixel_height))

    def obtain_breast_coordinates(self, datafile_filename: str) -> None:

        df = pd.read_table(Path(self.path.parent, datafile_filename), delimiter=";", decimal=",")
        df = df[df['File Path'].str.contains(str(self.path.as_posix()).lower())]
        self.left_nipple, self.left_breast_x_coordinates, self.left_breast_y_coordinates = get_breast_information(side="Left", df=df)
        self.right_nipple, self.right_breast_x_coordinates, self.right_breast_y_coordinates = get_breast_information(side="Right", df=df)


# %%

if __name__ == "__main__":
    # %%
    print("awef")
    images = []
    imgs = []
    white = (255, 255, 255)
    black = (0,0,0)
    for filename in os.listdir("420001 SMR"):
        if filename.__contains__("arme ned"):
            img_path = Path("420001 SMR", filename)
            test = Image_handler(path=img_path, datafile_filename='datafile.csv', color_for_cropping=white)
            test.plot_image()
            #images.append(test)
            #plt.show()
            test.crop_image()
            percentage = 0.1
            #test.resize_image(int(test.width * percentage), int(test.height * percentage))
            test.plot_image(add_breast_contours=True)
        

    # %%
