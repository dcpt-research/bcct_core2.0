# -*- coding: utf-8 -*-
"""
Created on Mon Aug 22 08:15:45 2022
@authors: Morten Sahlertz & Jacob Kjærager
"""
import os
import cv2
from sklearn.decomposition import PCA
from pathlib import Path
from dataimport import import_data, import_tempdata
from feature_engineering import *
from plotting import plot_temp_and_H2S
import matplotlib.pyplot as plt
from PIL import Image
from matplotlib import patches
import plotly.graph_objects as go
from plotly.offline import plot

if __name__ == "__main__":
    bb_height = 300
    bb_width = 300
    df = pd.read_table("./420001 SMR/datafile.csv", delimiter=";", decimal=",")
    left_group = df[df.columns[df.columns.str.contains("Left")]]
    all_nipple_left = df[left_group.columns[left_group.columns.str.contains("nipple")]]
    left_group_x_coordinate = df[left_group.columns[left_group.columns.str.contains("x")]]
    left_group_y_coordinate = df[left_group.columns[left_group.columns.str.contains("y")]]
    right_group = df[df.columns[df.columns.str.contains("Right")]]
    all_nipple_right = df[right_group.columns[right_group.columns.str.contains("nipple")]]
    right_group_x_coordinate = df[right_group.columns[right_group.columns.str.contains("x")]]
    right_group_y_coordinate = df[right_group.columns[right_group.columns.str.contains("y")]]
    armpit_le_red = []
    armpit_le_green = []
    armpit_le_blue = []
    armpit_ri_red = []
    armpit_ri_green = []
    armpit_ri_blue = []
    pca = PCA(n_components=3)
    img_text = []
    img_index = 0
    #for img_index in range(0, 6):
    img_text.append(img_index)
    test_file = "./420001 SMR/{}".format([filename for filename in os.listdir("./420001 SMR") if "arme ned" in filename][img_index])
    img = Image.open(test_file)
    nipple_right = {"x":all_nipple_right["Right nipple x"][img_index],
                    "y":all_nipple_right["Right nipple y"][img_index]}
    nipple_left = {"x": all_nipple_left["Left nipple x"][img_index],
                   "y" :all_nipple_left["Left nipple y"][img_index]}
    armpit_right = {"x": right_group_x_coordinate['Right contour x1'][img_index],
                    "y":right_group_y_coordinate['Right contour y1'][img_index]}
    armpit_left = {"x": left_group_x_coordinate['Left contour x1'][img_index],
                   "y":left_group_y_coordinate['Left contour y1'][img_index]}

    nipple_left_patch = img.crop((nipple_left["x"]-bb_width, nipple_left["y"]-bb_height,
                         nipple_left["x"]+bb_width, nipple_left["y"]+bb_height))
    nipple_right_patch = img.crop((nipple_right["x"]-bb_width, nipple_right["y"]-bb_height,
                         nipple_right["x"]+bb_width, nipple_right["y"]+bb_height))
    armpit_left_patch = img.crop((armpit_left["x"]-bb_width, armpit_left["y"]-bb_height,
                         armpit_left["x"]+bb_width, armpit_left["y"]+bb_height))
    armpit_right_patch = img.crop((armpit_right["x"]-bb_width, armpit_right["y"]-bb_height,
                         armpit_right["x"]+bb_width, armpit_right["y"]+bb_height))
    red1, green1, blue1 = armpit_left_patch.split()
    red2, green2, blue2 = armpit_right_patch.split()

    armpit_le_red.append(pd.DataFrame(np.asarray(red1)/255))
    armpit_le_green.append(pd.DataFrame(np.asarray(green1)/255)[0])
    armpit_le_blue.append(pd.DataFrame(np.asarray(blue1)/255)[0])
    armpit_ri_red.append(pd.DataFrame(np.asarray(red2)/255)[0])
    armpit_ri_green.append(pd.DataFrame(np.asarray(green2)/255)[0])
    armpit_ri_blue.append(pd.DataFrame(np.asarray(blue2)/255)[0])
    plt.imshow(nipple_left_patch)
    fig1, ax1 = plt.subplots()
    ax1.imshow(nipple_right_patch)
    fig2, ax2 = plt.subplots()
    ax2.imshow(armpit_left_patch)
    fig3, ax3 = plt.subplots()
    ax3.imshow(armpit_right_patch)
    fig4, ax4 = plt.subplots()
    plt.plot([armpit_right["x"], armpit_left["x"]], [armpit_right["y"], armpit_left["y"]], color="white", linewidth=3)
    print("width between armpits: {}pixels".format(armpit_right["x"] - armpit_left["x"]))
    ax4.imshow(img)
    # # Create a Rectangle patch
    rect_right_nipple = patches.Rectangle((nipple_right["x"]-(bb_width/2),
                                           nipple_right["y"]-(bb_height/2)),
                                           bb_height, bb_width, linewidth=1, edgecolor='r', facecolor='none')
    ax2.add_patch(rect_right_nipple)
    rect_left_nipple = patches.Rectangle((nipple_left["x"] - (bb_width / 2),
                                          nipple_left["y"] - (bb_height / 2)),
                                          bb_height, bb_width, linewidth=1, edgecolor='r', facecolor='none')
    ax2.add_patch(rect_left_nipple)
    rect_right_armpit = patches.Rectangle((armpit_right['x']-(bb_width/2),
                                           armpit_right['y']-(bb_height/2)),
                                           bb_height, bb_width, linewidth=1, edgecolor='r', facecolor='none')
    ax2.add_patch(rect_right_armpit)
    rect_left_armpit = patches.Rectangle((armpit_left['x']-(bb_width/2),
                                          armpit_left['y']-(bb_height/2)),
                                          bb_height, bb_width, linewidth=1, edgecolor='r', facecolor='none')
    ax2.add_patch(rect_left_armpit)
    plt.show()
    plt.scatter(right_group_x_coordinate.iloc[img_index],
                right_group_y_coordinate.iloc[img_index],
                s=25, c='red', marker='o')
    plt.scatter(left_group_x_coordinate.iloc[img_index],
                left_group_y_coordinate.iloc[img_index],
                s=25, c='blue', marker='o')
    plt.scatter(nipple_right["Right nipple x"][img_index],
                nipple_right["Right nipple y"][img_index],
                s=25, c='yellow', marker='o')
    plt.scatter(nipple_left["Left nipple x"][img_index],
                nipple_left["Left nipple y"][img_index],
                s=25, c='green', marker='o')
    print("awef")
    plt.show()
    print("aewf")
    # import numpy as np
    # import cv2
    # import matplotlib.pyplot as plt
    #
    # import torch
    # from torchvision.models.segmentation import deeplabv3_resnet101
    # from torchvision import transforms
    #
    #
    # def make_deeplab(device):
    #     deeplab = deeplabv3_resnet101(pretrained=True).to(device)
    #     deeplab.eval()
    #     return deeplab
    #
    #
    # device = torch.device("cpu")
    # deeplab = make_deeplab(device)
    #
    # img_orig = cv2.imread(test_file, 1)
    # plt.imshow(img_orig[:, :, ::-1])
    # plt.show()
    #
    # k = min(1.0, 1024 / max(img_orig.shape[0], img_orig.shape[1]))
    # img = cv2.resize(img_orig, None, fx=k, fy=k, interpolation=cv2.INTER_LANCZOS4)
    # deeplab_preprocess = transforms.Compose([
    #     transforms.ToTensor(),
    #     transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
    # ])
    # def apply_deeplab(deeplab, img, device):
    #     input_tensor = deeplab_preprocess(img)
    #     input_batch = input_tensor.unsqueeze(0)
    #     with torch.no_grad():
    #         output = deeplab(input_batch.to(device))['out'][0]
    #     output_predictions = output.argmax(0).cpu().numpy()
    #     return (output_predictions == 15)
    #
    #
    # mask = apply_deeplab(deeplab, img, device)
    #
    # plt.imshow(mask, cmap="gray")
    # plt.show()
    #
    #
    # fig = go.Figure(
    #     data= [
    #         go.Scatter3d(
    #             x=pd.DataFrame(armpit_ri_red)[0],
    #             y=pd.DataFrame(armpit_ri_red)[1],
    #             z=pd.DataFrame(armpit_ri_red)[2],
    #             mode='markers',
    #             text=img_text,
    #             marker=dict(
    #                 color="red"
    #             )
    #         ),
    #         go.Scatter3d(
    #             x=pd.DataFrame(armpit_ri_green)[0],
    #             y=pd.DataFrame(armpit_ri_green)[1],
    #             z=pd.DataFrame(armpit_ri_green)[2],
    #             mode='markers',
    #             text=img_text,
    #             marker=dict(
    #                 color="green"
    #             )
    #         ),
    #         go.Scatter3d(
    #             x=pd.DataFrame(armpit_ri_blue)[0],
    #             y=pd.DataFrame(armpit_ri_blue)[1],
    #             z=pd.DataFrame(armpit_ri_blue)[2],
    #             mode='markers',
    #             text=img_text,
    #             marker=dict(
    #                 color="blue"
    #             )
    #         ),
    #     ]
    # )
    #
    # plot(fig)
    # print("afew")
    site = "Site 1" #Must be part of filename for files of interest.
    new_data_import = True
    add_weatherdata = False

    if add_weatherdata:
        weather_suffix = "_with_weatherdata"
    else:
        weather_suffix = "_without_weatherdata"
    if new_data_import:
        VOI = 'Temperature_sensor'
        df_site = import_data(import_set=site)
        df_site = generate_one_minute_avg(df=df_site, VOI=VOI)
        df_site = calculate_below_avg_samples(df=df_site,
                                              VOI=VOI,
                                              below=True,
                                              smallest=True)
        df_site = calculate_below_avg_samples(df=df_site,
                                              VOI=VOI,
                                              below=False,
                                              smallest=False)
        df_site = get_max_h2s_within_temp_dips(df=df_site)
        #df_site = map_cyclical_features(df=df_site) #Used to map cyclical features to sin/cos values for better ML
        if add_weatherdata:
            df_site = import_tempdata(df=df_site, keep_columns=["Temperatur", 'Regn - minut', 'Date (UTC)'],
                                      filename="Silkeborg_vejr_data")
            df_site = df_site.rename(columns={"Temperatur": "Weather_temperature", "Regn - minut": "Weather_rain"})
        df_site.to_pickle("{}".format(Path("../", "large_files", "pickle_files", "df_{}_{}.pkl".format(site, weather_suffix))))
    else:
        df_site = pd.read_pickle(Path("../", "large_files", "pickle_files", "df_{}_{}.pkl".format(site, weather_suffix)))
    plot_temp_and_H2S(data=df_site, enable_weather_data=add_weatherdata)

