        
    def get_value_of_node(node):
    """ return node text or None """
    return node.text if node is not None else None    
        
        #def validate_keypoints(keypoints):
#    keypoint_keys = ["nipple_left_image_patch", "nipple_right_image_patch", "armpit_left_image_patch", "armpit_right_image_patch"]
#    for i in keypoint_keys:
#        keypoints[]
        def write_new_datafile(self, xml_filename: str) -> None:
        # Open the XML file and read it
        with open(Path(self.path.parent, 'BCCT.core.database.bct.xml'), 'r') as r:
            # Generate a new XML file with correct values
            with open(Path(self.path.parent, xml_filename), 'w') as w:
                for line in r:
                    # Check for mismatched tags
                    if 'ss:Data' in line:
                        line = line.replace('ss:Data', 'Data')
                    # Remove URL's
                    if ' xmlns="http://www.w3.org/TR/REC-html40">' in line:
                        line = line.replace(' xmlns="http://www.w3.org/TR/REC-html40"', '')
                    # Remove sup for proper names
                    if '<Sup>' in line:
                        line = line.replace('<Sup>', '')
                        line = line.replace('</Sup>', '')
                    # Remove sub for proper names
                    if '<Sub>' in line:
                        line = line.replace('<Sub>', '')
                        line = line.replace('</Sub>', '')
                    w.write(line)
                        
    def read_xml_file(self, xml_filename: str = 'datafile.xml') -> pd.DataFrame:
        
        # https://stackoverflow.com/questions/61548942/attempting-to-parse-an-xls-xml-file-using-python
        # Check if file exist, otherwise create it
        if not os.path.isfile(Path(self.path.parent, xml_filename)):
            print('Making new xml file!')
            self.write_new_datafile(xml_filename = xml_filename)
        # Namespace for the XML
        ns = {"doc": "urn:schemas-microsoft-com:office:spreadsheet"}
        # Generate Tree for XML
        tree = ET.parse(Path(self.path.parent, xml_filename))
        root = tree.getroot()
        # Append the read data in rows
        data = []
        for i, node in enumerate(root.findall('.//doc:Row', ns)):
            row = []
            for i in range(len(node)):
                row.append(get_value_of_node(node.find('doc:Cell[{}]/doc:Data'.format(i+1), ns)))
            data.append(row)
        # Change data to a DataFrame
        df = pd.DataFrame(data)
        df = pd.DataFrame(df.values[1:], columns=df.iloc[0])   
        # Change values to float for columns without strings
        for column in df:
            if 'path' in str(column).lower() or 'date' in str(column).lower() or 'software' in str(column).lower():
                pass
            else:
                df[column] = df[column].astype(float)
        # Rename 'Left nipple x' duplicate
        if df['Left nipple x'].iloc[0][0] < df['Left nipple x'].iloc[0][1]:
            rename_dict = {'Left nipple x': ['Left nipple x', 'Right nipple x']}
        else:
            rename_dict = {'Left nipple x': ['Right nipple x', 'Left nipple x']}
        df = df.rename(columns=lambda c: rename_dict[c].pop(0) if c in rename_dict.keys() else c)
        # Return the DataFrame
        return df

        roots = []
        roots.append(os.path.join(dirname, 'Jasper', 'DBCGRT', 'Aalborg', 'BCCT HYPO Aalborg 03.2018'))
        roots.append(os.path.join(dirname, 'Jasper', 'DBCGRT', 'Aarhus', 'BCCT HYPO Aarhus 03.2018'))
        roots.append(os.path.join(dirname, 'Jasper', 'DBCGRT', 'Dresden CGC', 'BCCT HYPO Dresden CGC 03.2018'))
        roots.append(os.path.join(dirname, 'Jasper', 'DBCGRT', 'Odense', 'BCCT HYPO Odense 03.2018'))
        roots.append(os.path.join(dirname, 'Jasper', 'DBCGRT', 'Vejle', 'BCCT HYPO Vejle 03.2018'))


        filename = 'BCCT.core.database.bct.xml'
        errors = []
        correct_paths = []
        cnt_true = 0
        for root in roots:
            for path, subdirs, files in os.walk(root):
                for name in files:
                    if name.lower() == filename.lower():
                        df = pd.DataFrame()                                
                        filepath = os.path.join(path, name)
                        # print(filepath)
                        with open(filepath, "r") as file:
                            # Read each line in the file, readlines() returns a list of lines
                            content = file.readlines()
                            # Combine the lines in the list into a string
                            content = "".join(content)
                            bs_content = bs(content, "lxml")

                            for worksheet in bs_content.find_all('worksheet'):
                                rows = worksheet.find_all('row')
                                # header_row = worksheet.select('table > row')[0]
                                header_row = rows[0]
                                headers = header_row.find_all('cell')
                                #find the cell which contains the 'repeated path' tag
                                #header = header_row.find('cell', text='repeated path')
                                
                                for i in range(1,len(rows)):
                                    for header in headers:
                                        dataline = rows[i]
                                        data = dataline.find_all('cell')
                                        txt = data[headers.index(header)].text.strip()
                                        if header.text.strip() == "repeated path": 
                                            folder = os.path.basename(os.path.dirname(txt))
                                        
                                            filestr = os.path.join(os.path.dirname(filepath), os.path.basename(txt))
                                            if not(os.path.exists(filestr)):
                                            #    print(filestr)
                                                corruptfilename = os.path.basename(txt)
                                                #replace the superscript 2 with a dot 
                                                corruptfilename = corruptfilename.replace(u"\u00AE", '.')
                                                #replace the superscript 2 with a r                             
                                                corruptfilename = corruptfilename.replace(u"\u00B2", 'r')
                                                #replace the non gap space with a full space
                                                corruptfilename = corruptfilename.replace(u"\u00A0", ' ')
                                                filestr = os.path.join(os.path.dirname(filepath),corruptfilename)
                                            #print(filestr, os.path.exists(filestr))
                                            if os.path.exists(filestr):
                                                cnt_true +=1
                                                #correct_paths.append(filestr)
                                                df.loc[i, header.text.strip()] = filestr
                                            else:
                                                #errors.append(filestr)
                                                df.loc[i, header.text.strip()] = np.NaN
                                        else:
                                            df.loc[i, header.text.strip()] = data[headers.index(header)].text.strip()
                                    cnt += 1
                                    if cnt%100 == 0:
                                        print("wuhuu 100 patients more")
                        collected_df.append(df)

        collected_bcct_core_data = pd.concat(collected_df).reset_index(drop=True)
        collected_bcct_core_data["repeated path"] = collected_bcct_core_data["repeated path"].str.rstrip().str.lower() 
        suffix = "from_admin_file" 
        merged = collected_bcct_core_data.merge(df_administration_files, on="repeated path", suffixes=('', suffix))
        merged = merged.drop(columns=(collected_bcct_core_data.columns.drop(["Overall classification", "repeated path"]) + suffix)) #Add to this list if more data from admin file is required. 
        pd.to_pickle(merged, save_directory+ "\\merged_bcct_core_and_admin_files.pkl")

    #else:
     #    merged = pd.read_pickle(save_directory+"\\merged_bcct_core_and_admin_files.pkl")


    for folder in os.listdir(photo_folder):
        for filename in os.listdir("{}/{}".format(photo_folder, folder)):
            if frontfacing_key in filename:
                img_path = Path(photo_folder, filename)
                test = Image_handler(path=img_path, datafile_filename=datafile_name, color_for_cropping=white)
                test.plot_image()
                percentage = 0.2
                test.resize_image(int(test.width * percentage), int(test.height * percentage))
                test.plot_image()
                keypoints = test.generate_keypoint_patches(radius_weight=0.5)
                collected_keypoints[filename] = keypoints
                nipple_l.append(cv.cvtColor(keypoints["nipple_left_image_patch"], cv.COLOR_BGR2RGB))
                nipple_r.append(cv.cvtColor(keypoints["nipple_right_image_patch"], cv.COLOR_BGR2RGB))
                armpit_l.append(cv.cvtColor(keypoints["armpit_left_image_patch"], cv.COLOR_BGR2RGB))
                armpit_r.append(cv.cvtColor(keypoints["armpit_right_image_patch"], cv.COLOR_BGR2RGB))
                naming.append(filename)
                #error_list = validate_keypoints(keypoints)
                plot_keypoints(keypoints)
                #images.append(test)
                #plt.show()
                test.crop_image()
                percentage = 0.1
                #test.resize_image(int(test.width * percentage), int(test.height * percentage))
                test.plot_image(add_breast_contours=True)

    
    fig = go.Figure(
        data=[
            go.Bar(
                x=np.linalg.norm((pca_matrix-centroid), axis=1),
                text=naming, #text is distance from centroid 
            )
        ]
    )
    plot(fig)
    print("af")
            
    validation_generator = train_datagen.flow_from_dataframe(
        dataframe=df, # same directory as training data
        shuffle=True,
        x_col="new_abs_path", 
        y_col="Overall classification", 
        target_size=(600,600), 
        class_mode="categorical",
        batch_size=7, 
        color_mode="rgb",
        subset='validation') # set as validation data

    
    
    df = pd.read_excel("../../../Jasper/DBCGRT/Aarhus/BCCT HYPO Aarhus 03.2018.xlsx")
    df_with_path = df[~df["repeated path"].isna()]
    df_with_path["Image_filename"] = df_with_path[~df_with_path["repeated path"].isna()]["repeated path"].str.split("/").str.get(-1)
    grouped = dict(list(df_with_path.groupby("Patient folder")))
    inte = 1
    cv.imread("../../../Jasper/DBCGRT/{}/BCCT HYPO {} 03.2018/{}/{}".format(df_with_path["Institution"].iloc[inte], df_with_path["Institution"].iloc[inte], 
        df_with_path["Patient folder"].iloc[inte], df_with_path["Image_filename"].iloc[inte].as_posix()))
    df_total = pd.DataFrame()
    #Pathing 
    errorlist=[]
    hospitals = ["Aarhus", "Dresden CGC", "Odense", "Vejle"] #422030 BS 422030 BS ["Aalborg", "Aarhus", "Dresden CGC", "Odense", "Vejle"]
    for hospital in hospitals:
        print(hospital)
        hospital_folder = "E:\Jasper\DBCGRT\{}\BCCT HYPO {} 03.2018".format(hospital, hospital)
        for hospital_subfolder in next(os.walk(hospital_folder))[1]:
            hospital_sub_sub_folder = hospital_folder + "\\" + "{}".format(hospital_subfolder)
            sub_sub_directories = next(os.walk(hospital_sub_sub_folder))[1]
            if sub_sub_directories:
                if sub_sub_directories[0] == "Sendt": #Vejle specific filter
                    sub_sub_directory = hospital_sub_sub_folder + "\\" + sub_sub_directories[0]
                    for sub_sub_sub_folders in os.listdir(sub_sub_directory):
                        if "BCCT.core.database.bct.xml" in os.listdir(Path(sub_sub_directory, sub_sub_sub_folders)) :
                            df = read_xml(path=Path("{}\\{}\BCCT.core.database.bct.xml".format(sub_sub_directory, sub_sub_sub_folders)))
                            df_total = pd.concat([df_total, df]).reset_index(drop=True)
                        else:
                            errorlist.append(Path(sub_sub_directory, sub_sub_sub_folders))
                else:
                    errorlist.append(Path(hospital_sub_sub_folder, sub_sub_directories[0]))
            elif("BCCT.core.database.bct.xml" in os.listdir(Path("E:\Jasper\DBCGRT\{}\BCCT HYPO {} 03.2018\{}".format(hospital, hospital, hospital_subfolder)))):
                if ("E:\Jasper\DBCGRT\{}\BCCT HYPO {} 03.2018\{}".format(hospital, hospital, hospital_subfolder) == 'E:\\Jasper\\DBCGRT\\Aarhus\\BCCT HYPO Aarhus 03.2018\\AMA 280631') or \
                   ("E:\Jasper\DBCGRT\{}\BCCT HYPO {} 03.2018\{}".format(hospital, hospital, hospital_subfolder) == 'E:\\Jasper\\DBCGRT\\Aarhus\\BCCT HYPO Aarhus 03.2018\\EKC 090741') or \
                   ("E:\Jasper\DBCGRT\{}\BCCT HYPO {} 03.2018\{}".format(hospital, hospital, hospital_subfolder) == 'E:\\Jasper\\DBCGRT\\Aarhus\\BCCT HYPO Aarhus 03.2018\\IDP 131146') or \
                   ("E:\Jasper\DBCGRT\{}\BCCT HYPO {} 03.2018\{}".format(hospital, hospital, hospital_subfolder) == 'E:\\Jasper\\DBCGRT\\Aarhus\\BCCT HYPO Aarhus 03.2018\\DFH 270162') or \
                   ("E:\Jasper\DBCGRT\{}\BCCT HYPO {} 03.2018\{}".format(hospital, hospital, hospital_subfolder) == 'E:\\Jasper\\DBCGRT\\Aarhus\\BCCT HYPO Aarhus 03.2018\\KBN 161251') or \
                   ("E:\Jasper\DBCGRT\{}\BCCT HYPO {} 03.2018\{}".format(hospital, hospital, hospital_subfolder) == 'E:\\Jasper\\DBCGRT\\Aarhus\\BCCT HYPO Aarhus 03.2018\\KC 040438') or \
                   ("E:\Jasper\DBCGRT\{}\BCCT HYPO {} 03.2018\{}".format(hospital, hospital, hospital_subfolder) == "E:\\Jasper\\DBCGRT\\Aarhus\\BCCT HYPO Aarhus 03.2018\\BR 200955"):
                    print("Wrong encoding")
                    errorlist.append("E:\Jasper\DBCGRT\{}\BCCT HYPO {} 03.2018\{}".format(hospital, hospital, hospital_subfolder))
                else:
                    df = read_xml(path=Path("E:\Jasper\DBCGRT\{}\BCCT HYPO {} 03.2018\{}\BCCT.core.database.bct.xml".format(hospital, hospital, hospital_subfolder)))
                    df_total = pd.concat([df_total, df]).reset_index(drop=True)
            else:
                print("Error in: Hospital: {}, Subfolder: {}".format(hospital, hospital_subfolder))
                errorlist.append(Path("E:\Jasper\DBCGRT\{}\BCCT HYPO {} 03.2018\{}".format(hospital, hospital, hospital_subfolder)))
            #print("awef")
            
    print("awef")
    
    #df = pd.read_excel("../../../Jasper/DBCGRT/Aalborg/BCCT HYPO Aalborg 03.2018.xlsx")
    #df[df['Problem 0=No 1=Yes'] == 0].reset_index(drop=True)
    path = "../../../Jasper/DBCGRT/Aalborg/BCCT HYPO Aalborg 03.2018"
    for folders in os.listdir(path):
        for photo_text in os.listdir("{}/{}".format(path, folders)):
            if "b.jpg" in photo_text:   
                n.append(photo_text)
    print(len(n))
#["422010 USB", "422030 BS", "422010 USB", "423018 CRN", "603017", "604009", "604014", "604020", "605003", "605016", "605043", "605045", "605051", 
#                        "606005", "607008", "608008", "608020", "608023", "608031", "609001","609004", "609006"]
    images = []
    armpit_l = []
    armpit_r = []
    nipple_l = []
    nipple_r = []
    naming= [] 
    collected_keypoints = {}
    white = (255, 255, 255)
    black = (0,0,0)
    photo_folder = "../../../Jasper/DBCGRT/Aalborg/BCCT HYPO Aalborg 03.2018/Hypo 8000"


    datafile_name_aalborg = "../../../Jasper/DBCGRT/Aalborg/BCCT HYPO Aalborg 03.2018.xlsx"
    datafile_name_aarhus = "../../../Jasper/DBCGRT/Aarhus/BCCT HYPO Aarhus 03.2018.xlsx"
    datafile_name_dredsen = "../../../Jasper/DBCGRT/Dresden CGC/BCCT HYPO Dresden CGC 03.2018.xlsx"
    datafile_name_odense = "../../../Jasper/DBCGRT/Odense/BCCT HYPO Odense 03.2018.xlsx"
    datafile_name_vejle = "../../../Jasper/DBCGRT/Vejle/BCCT HYPO Vejle 03.2018.xlsx"
    df_aalborg = pd.read_excel(datafile_name_aalborg)
    df_aarhus = pd.read_excel(datafile_name_aarhus)
    df_dredsen = pd.read_excel(datafile_name_dredsen)
    df_odense = pd.read_excel(datafile_name_odense)
    df_vejle = pd.read_excel(datafile_name_vejle)


    print("awe")

    hospital_folder = "../../../Jasper/DBCGRT/Aalborg/BCCT HYPO Aalborg 03.2018"
    datafile_name = "BCCT.core.database.bct.xml"
    frontfacing_key = "b.jpg"



for line in r:
    if counter == 256:
        print("fae")
    # Check for mismatched tags
    if 'ss:Data' in line:
        line = line.replace('ss:Data', 'Data')
    # Remove URL's
    if ' xmlns="http://www.w3.org/TR/REC-html40">' in line:
        line = line.replace(' xmlns="http://www.w3.org/TR/REC-html40"', '')
    # Remove sup for proper names
    if '<Sup>' in line:
        line = line.replace('<Sup>', '')
        line = line.replace('</Sup>', '')
    # Remove sub for proper names
    if '<Sub>' in line:
        line = line.replace('<Sub>', '')
        line = line.replace('</Sub>', '')
    if 'æ' in line:
        line = line.replace("æ", "xF8")
    if 'ø' in line: 
        line = line.replace("ø", "xF8")
    if 'å':
        line = line.replace("å", "0xA5oijgaoijrgoijfdg")

















bb_height = 300
bb_width = 300
df = pd.read_table("./420001 SMR/datafile.csv", delimiter=";", decimal=",")
left_group = df[df.columns[df.columns.str.contains("Left")]]
all_nipple_left = df[left_group.columns[left_group.columns.str.contains("nipple")]]
left_group_x_coordinate = df[left_group.columns[left_group.columns.str.contains("x")]]
left_group_y_coordinate = df[left_group.columns[left_group.columns.str.contains("y")]]
right_group = df[df.columns[df.columns.str.contains("Right")]]
all_nipple_right = df[right_group.columns[right_group.columns.str.contains("nipple")]]
right_group_x_coordinate = df[right_group.columns[right_group.columns.str.contains("x")]]
right_group_y_coordinate = df[right_group.columns[right_group.columns.str.contains("y")]]
armpit_le_red = []
armpit_le_green = []
armpit_le_blue = []
armpit_ri_red = []
armpit_ri_green = []
armpit_ri_blue = []
pca = PCA(n_components=3)
img_text = []
img_index = 0
# for img_index in range(0, 6):
img_text.append(img_index)
test_file = "./420001 SMR/{}".format(
    [filename for filename in os.listdir("./420001 SMR") if "arme ned" in filename][img_index])
img = Image.open(test_file)

nipple_right = {"x": all_nipple_right["Right nipple x"][img_index],
                "y": all_nipple_right["Right nipple y"][img_index]}
nipple_left = {"x": all_nipple_left["Left nipple x"][img_index],
                "y": all_nipple_left["Left nipple y"][img_index]}
armpit_right = {"x": right_group_x_coordinate['Right contour x1'][img_index],
                "y": right_group_y_coordinate['Right contour y1'][img_index]}
armpit_left = {"x": left_group_x_coordinate['Left contour x1'][img_index],
                "y": left_group_y_coordinate['Left contour y1'][img_index]}

nipple_left_patch = img.crop((nipple_left["x"] - bb_width, nipple_left["y"] - bb_height,
                                nipple_left["x"] + bb_width, nipple_left["y"] + bb_height))
nipple_right_patch = img.crop((nipple_right["x"] - bb_width, nipple_right["y"] - bb_height,
                                nipple_right["x"] + bb_width, nipple_right["y"] + bb_height))
armpit_left_patch = img.crop((armpit_left["x"] - bb_width, armpit_left["y"] - bb_height,
                                armpit_left["x"] + bb_width, armpit_left["y"] + bb_height))
armpit_right_patch = img.crop((armpit_right["x"] - bb_width, armpit_right["y"] - bb_height,
                                armpit_right["x"] + bb_width, armpit_right["y"] + bb_height))
red1, green1, blue1 = armpit_left_patch.split()
red2, green2, blue2 = armpit_right_patch.split()

armpit_le_red.append(pd.DataFrame(np.asarray(red1) / 255))
armpit_le_green.append(pd.DataFrame(np.asarray(green1) / 255)[0])
armpit_le_blue.append(pd.DataFrame(np.asarray(blue1) / 255)[0])

armpit_ri_red.append(pd.DataFrame(np.asarray(red2) / 255)[0])
armpit_ri_green.append(pd.DataFrame(np.asarray(green2) / 255)[0])
armpit_ri_blue.append(pd.DataFrame(np.asarray(blue2) / 255)[0])
plt.imshow(nipple_left_patch)
fig1, ax1 = plt.subplots()
ax1.imshow(nipple_right_patch)
fig2, ax2 = plt.subplots()
ax2.imshow(armpit_left_patch)
fig3, ax3 = plt.subplots()
ax3.imshow(armpit_right_patch)
fig4, ax4 = plt.subplots()
plt.plot([armpit_right["x"], armpit_left["x"]], [armpit_right["y"], armpit_left["y"]], color="white", linewidth=3)
print("width between armpits: {}pixels".format(armpit_right["x"] - armpit_left["x"]))
ax4.imshow(img)
# # Create a Rectangle patch
rect_right_nipple = patches.Rectangle((nipple_right["x"] - (bb_width / 2),
                                        nipple_right["y"] - (bb_height / 2)),
                                        bb_height, bb_width, linewidth=1, edgecolor='r', facecolor='none')
ax2.add_patch(rect_right_nipple)
rect_left_nipple = patches.Rectangle((nipple_left["x"] - (bb_width / 2),
                                        nipple_left["y"] - (bb_height / 2)),
                                        bb_height, bb_width, linewidth=1, edgecolor='r', facecolor='none')
ax2.add_patch(rect_left_nipple)
rect_right_armpit = patches.Rectangle((armpit_right['x'] - (bb_width / 2),
                                        armpit_right['y'] - (bb_height / 2)),
                                        bb_height, bb_width, linewidth=1, edgecolor='r', facecolor='none')
ax2.add_patch(rect_right_armpit)
rect_left_armpit = patches.Rectangle((armpit_left['x'] - (bb_width / 2),
                                        armpit_left['y'] - (bb_height / 2)),
                                        bb_height, bb_width, linewidth=1, edgecolor='r', facecolor='none')
ax2.add_patch(rect_left_armpit)
#
plt.show()
plt.imshow(img)
plt.scatter(right_group_x_coordinate.iloc[img_index],
            right_group_y_coordinate.iloc[img_index],
            s=25, c='red', marker='o')
plt.scatter(left_group_x_coordinate.iloc[img_index],
            left_group_y_coordinate.iloc[img_index],
            s=25, c='blue', marker='o')
plt.scatter(all_nipple_right["Right nipple x"][img_index],
            all_nipple_right["Right nipple y"][img_index],
            s=25, c='yellow', marker='o')
plt.scatter(all_nipple_left["Left nipple x"][img_index],
            all_nipple_left["Left nipple y"][img_index],
            s=25, c='green', marker='o')
print("awef")
plt.show()
print("aewf")

# %%

images = []

white = (255, 255, 255)

test = Image_handler(Path("420001 SMR", "420001 091113 arme ned.jpg"), white)
test.plot_image()
images.append(test)
plt.show()
test.crop_image()
percentage = 0.2
test.resize_image(int(test.width * percentage), int(test.height * percentage))

test.plot_image_with_contours()
print("afew")

# %%

import xml.etree.ElementTree as ET
import pandas as pd

xml_data = open("420001 SMR/BCCT.core.database.bct.xml", 'r').read()  # Read file
root = ET.XML(xml_data)  # Parse XML

data = []
cols = []
for i, child in enumerate(root):
data.append([subchild.text for subchild in child])
cols.append(child.tag)

# %%

df1 = pd.read_xml("BCCT.core.database.bct.xml")
df2 = pd.read_excel("BCCT.core.database.bct.xml")
df_csv = df1.to_csv()
# df = pd.DataFrame(data).T  # Write in DF and transpose it
# df.columns = cols  # Update column names
# print(df)

# xml_data = open("420001 SMR/BCCT.core.database.bct.xml", 'r').read()  # Read file

# test_xml = pd.read_xml(xml_data)

# import numpy as np
# import cv2
# import matplotlib.pyplot as plt
#
# import torch
# from torchvision.models.segmentation import deeplabv3_resnet101
# from torchvision import transforms
#
#
# def make_deeplab(device):
#     deeplab = deeplabv3_resnet101(pretrained=True).to(device)
#     deeplab.eval()
#     return deeplab
#
#
# device = torch.device("cpu")
# deeplab = make_deeplab(device)
#
# img_orig = cv2.imread(test_file, 1)
# plt.imshow(img_orig[:, :, ::-1])
# plt.show()
#
# k = min(1.0, 1024 / max(img_orig.shape[0], img_orig.shape[1]))
# img = cv2.resize(img_orig, None, fx=k, fy=k, interpolation=cv2.INTER_LANCZOS4)
# deeplab_preprocess = transforms.Compose([
#     transforms.ToTensor(),
#     transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
# ])
# def apply_deeplab(deeplab, img, device):
#     input_tensor = deeplab_preprocess(img)
#     input_batch = input_tensor.unsqueeze(0)
#     with torch.no_grad():
#         output = deeplab(input_batch.to(device))['out'][0]
#     output_predictions = output.argmax(0).cpu().numpy()
#     return (output_predictions == 15)
#
#
# mask = apply_deeplab(deeplab, img, device)
#
# plt.imshow(mask, cmap="gray")
# plt.show()
#
#
# fig = go.Figure(
#     data= [
#         go.Scatter3d(
#             x=pd.DataFrame(armpit_ri_red)[0],
#             y=pd.DataFrame(armpit_ri_red)[1],
#             z=pd.DataFrame(armpit_ri_red)[2],
#             mode='markers',
#             text=img_text,
#             marker=dict(
#                 color="red"
#             )
#         ),
#         go.Scatter3d(
#             x=pd.DataFrame(armpit_ri_green)[0],
#             y=pd.DataFrame(armpit_ri_green)[1],
#             z=pd.DataFrame(armpit_ri_green)[2],
#             mode='markers',
#             text=img_text,
#             marker=dict(
#                 color="green"
#             )
#         ),
#         go.Scatter3d(
#             x=pd.DataFrame(armpit_ri_blue)[0],
#             y=pd.DataFrame(armpit_ri_blue)[1],
#             z=pd.DataFrame(armpit_ri_blue)[2],
#             mode='markers',
#             text=img_text,
#             marker=dict(
#                 color="blue"
#             )
#         ),
#     ]
# )
#
# plot(fig)
# print("afew")
# site = "Site 1" #Must be part of filename for files of interest.
# new_data_import = True
# add_weatherdata = False

# if add_weatherdata:
#     weather_suffix = "_with_weatherdata"
# else:
#     weather_suffix = "_without_weatherdata"
# if new_data_import:
#     VOI = 'Temperature_sensor'
#     df_site = import_data(import_set=site)
#     df_site = generate_one_minute_avg(df=df_site, VOI=VOI)
#     df_site = calculate_below_avg_samples(df=df_site,
#                                           VOI=VOI,
#                                           below=True,
#                                           smallest=True)
#     df_site = calculate_below_avg_samples(df=df_site,
#                                           VOI=VOI,
#                                           below=False,
#                                           smallest=False)
#     df_site = get_max_h2s_within_temp_dips(df=df_site)
#     #df_site = map_cyclical_features(df=df_site) #Used to map cyclical features to sin/cos values for better ML
#     if add_weatherdata:
#         df_site = import_tempdata(df=df_site, keep_columns=["Temperatur", 'Regn - minut', 'Date (UTC)'],
#                                   filename="Silkeborg_vejr_data")
#         df_site = df_site.rename(columns={"Temperatur": "Weather_temperature", "Regn - minut": "Weather_rain"})
#     df_site.to_pickle("{}".format(Path("../", "large_files", "pickle_files", "df_{}_{}.pkl".format(site, weather_suffix))))
# else:
#     df_site = pd.read_pickle(Path("../", "large_files", "pickle_files", "df_{}_{}.pkl".format(site, weather_suffix)))
# plot_temp_and_H2S(data=df_site, enable_weather_data=add_weatherdata)

# %%

# img = cv.imread("./420001 SMR/420001 101217 arme op.jpg")
# # img = cv.imread("./420001 SMR/420001 091113 arme ned.jpg")

# white = (255, 255, 255)
# black = (0, 0, 0)

# img_no_bg = remove_background(img, color=black, threshold=0.6)

# img_cropped = crop_image(img, img_no_bg, color=black)

# percentage = 0.2
# img_cropped = cv.resize(img_cropped,
#                         (int(img_cropped.shape[1]*percentage),
#                           int(img_cropped.shape[0]*percentage)))

# # img_cropped, left_group_x_coordinate, left_group_y_coordinate, right_group_x_coordinate, right_group_y_coordinate = resize_image(img_cropped,
# #                                                                                                                                 int(img_cropped.shape[1]*percentage),
# #                                                                                                                                 int(img_cropped.shape[0]*percentage),
# #                                                                                                                                 left_group_x_coordinate,
# #                                                                                                                                 left_group_y_coordinate,
# #                                                                                                                                 right_group_x_coordinate,
# #                                                                                                                                 right_group_y_coordinate)


# img = cv.resize(img,
#                 (int(img.shape[1]*percentage),
#                   int(img.shape[0]*percentage)))


# cv.imshow("Cropped Image", img_cropped)
# cv.imshow("Image", img)

# cv.waitKey(0)
# cv.destroyAllWindows()


# %%


# img = cv.imread("./420001 SMR/420001 091113 arme ned.jpg")

# percentage = 0.2

# img, left_group_x, left_group_y, right_group_x, right_group_y = resize_image(img,
#                                                                              int(img.shape[1]*percentage),
#                                                                              int(img.shape[0]*percentage),
#                                                                              left_group_x_coordinate,
#                                                                              left_group_y_coordinate,
#                                                                              right_group_x_coordinate,
#                                                                              right_group_y_coordinate)

# plt.imshow(img)
# plt.scatter(right_group_x.iloc[img_index],
#             right_group_y.iloc[img_index],
#             s=25, c='red', marker='o')
# plt.scatter(left_group_x.iloc[img_index],
#             left_group_y.iloc[img_index],
#             s=25, c='blue', marker='o')
# plt.show()


# def remove_background(image, color, threshold):

#     # image = cv.imread(image,cv.IMREAD_UNCHANGED)
#     segmentor = SelfiSegmentation()
#     image_no_background = segmentor.removeBG(image,
#                                              imgBg=color,
#                                              threshold=threshold)

#     return image_no_background

# def crop_image(image, image_no_background, color) -> np.array:
#     # Generate variables
#     pixel_count_l = 0
#     pixel_count_r = 0
#     # Run through the image from the right hand side
#     for i in range(image_no_background.shape[1]):
#         if np.all(image_no_background[:, i] == color):
#             pixel_count_r += 1
#         else:
#             break
#     # Run through the image from the left hand side
#     for i in range(image_no_background.shape[1]-1, 0, -1):
#         if np.all(image_no_background[:, i] == color):
#             pixel_count_l += 1
#         else:
#             break
#     # Return image
#     return image[:, pixel_count_r:-pixel_count_l]

# def resize_image(image: np.array,
#                  pixel_width: int,
#                  pixel_height: int,
#                  left_group_x,
#                  left_group_y,
#                  right_group_x,
#                  right_group_y):

#     width_percentage = pixel_width / image.shape[1]
#     height_percentage = pixel_height / image.shape[0]

#     left_group_x = left_group_x * width_percentage
#     right_group_x = right_group_x * width_percentage
#     left_group_y = left_group_y * height_percentage
#     right_group_y = right_group_y * height_percentage

#     image_resized = cv.resize(image, (pixel_width, pixel_height))

#     return image_resized, left_group_x, left_group_y, right_group_x, right_group_y