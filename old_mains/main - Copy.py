# -*- coding: utf-8 -*-
"""
Created on Mon Aug 22 08:15:45 2022
@authors: Morten Sahlertz & Jacob Kjærager
"""


import warnings
warnings.filterwarnings("ignore")


import os
import html
import cv2 as cv
import numpy as np
import pandas as pd
from math import ceil
from operator import mul
import SimpleITK as sitk
from pathlib import Path
from cProfile import label
from functools import reduce
from matplotlib import patches
from codecs import EncodedFile
import matplotlib.pyplot as plt
from plotly.offline import plot
from PIL import Image, ImageDraw
import plotly.graph_objects as go
import xml.etree.ElementTree as ET
from bs4 import BeautifulSoup as bs
from sklearn.decomposition import PCA
from typing import Dict, List, Tuple, Union
from distutils.ccompiler import new_compiler
from dataimport import import_data, import_tempdata
from cvzone.SelfiSegmentationModule import SelfiSegmentation


def manipulate_string(encoded_string: str, decode_dict: dict) -> str:

    for i in decode_dict.keys():
        encoded_string = encoded_string.replace(i, decode_dict[i])
    return encoded_string



def get_value_of_node(node):
    """ return node text or None """
    return node.text if node is not None else None

#def validate_keypoints(keypoints):
#    keypoint_keys = ["nipple_left_image_patch", "nipple_right_image_patch", "armpit_left_image_patch", "armpit_right_image_patch"]
#    for i in keypoint_keys:
#        keypoints[]
def write_new_file(new_xml_filename: str, path: Path) -> None:
# Open the XML file and read it
    counter = 0 

    with open(Path(path.parent, 'BCCT.core.database.bct.xml'), 'r', encoding="ANSI") as r:
        # Generate a new XML file with correct values
        with open(Path(path.parent, new_xml_filename), 'w') as w:
            for line in r:
                if counter == 256:
                    print("fae")
                # Check for mismatched tags
                if 'ss:Data' in line:
                    line = line.replace('ss:Data', 'Data')
                # Remove URL's
                if ' xmlns="http://www.w3.org/TR/REC-html40">' in line:
                    line = line.replace(' xmlns="http://www.w3.org/TR/REC-html40"', '')
                # Remove sup for proper names
                if '<Sup>' in line:
                    line = line.replace('<Sup>', '')
                    line = line.replace('</Sup>', '')
                # Remove sub for proper names
                if '<Sub>' in line:
                    line = line.replace('<Sub>', '')
                    line = line.replace('</Sub>', '')
                w.write(line)

                
def read_xml(path: Path, xml_filename: str = 'datafile.xml') -> pd.DataFrame:
    # https://stackoverflow.com/questions/61548942/attempting-to-parse-an-xls-xml-file-using-python
    # Check if file exist, otherwise create it
    if os.path.isfile(Path(path.parent, xml_filename)):
        print('Making new xml file!')
        write_new_file(new_xml_filename = xml_filename, path=path)
    # Namespace for the XML
    ns = {"doc": "urn:schemas-microsoft-com:office:spreadsheet"}
    # Generate Tree for XML 
    xmlp = ET.XMLParser(encoding="ANSI")
    tree =ET.parse(Path(path.parent, xml_filename), xmlp)
    root = tree.getroot()
    # Append the read data in rows
    data = []
    for i, node in enumerate(root.findall('.//doc:Row', ns)):
        row = []
        for i in range(len(node)):
            row.append(get_value_of_node(node.find('doc:Cell[{}]/doc:Data'.format(i+1), ns)))
        data.append(row)
    # Change data to a DataFrame
    df = pd.DataFrame(data)
    df = pd.DataFrame(df.values[1:], columns=df.iloc[0])  
    df["repeated path"] = df["File Path"] #Fixes the \n error
    df["repeated path"] = (path.parent.as_posix()+ "//" +df["File Path"].str.split("/").str.get(-1)).map(Path) 

    # Change values to float for columns without strings
    for column in df:
        if 'path' in str(column).lower() or 'date' in str(column).lower() or 'software' in str(column).lower():
            pass
        else:
            df[column] = df[column].astype(float)
    # Rename 'Left nipple x' duplicate
    if df['Left nipple x'].iloc[0][0] < df['Left nipple x'].iloc[0][1]:
        rename_dict = {'Left nipple x': ['Left nipple x', 'Right nipple x']}
    else:
        rename_dict = {'Left nipple x': ['Right nipple x', 'Left nipple x']}
    df = df.rename(columns=lambda c: rename_dict[c].pop(0) if c in rename_dict.keys() else c)
    # Return the DataFrame
    return df


def get_breast_information(side: str, df: pd.DataFrame) -> List[Union[dict, pd.DataFrame, pd.DataFrame]]:
        right_group = df[df.columns[df.columns.str.contains(side)]]
        df_nipple = df[right_group.columns[right_group.columns.str.contains("nipple")]]
        nipple = {"x": df_nipple["{} nipple x".format(side)],
                  "y": df_nipple["{} nipple y".format(side)]}
        breast_x_coordinates = df[right_group.columns[right_group.columns.str.contains("x")]].drop(columns=['{} nipple x'.format(side)])
        breast_y_coordinates = df[right_group.columns[right_group.columns.str.contains("y")]].drop(columns=['{} nipple y'.format(side)])
        return [nipple, breast_x_coordinates, breast_y_coordinates]

def remove_background(color_for_cropping: tuple, image: any, threshold: float = 0.4) -> np.array:

    segmentor = SelfiSegmentation()
    image_no_background = segmentor.removeBG(image,
                                             imgBg=color_for_cropping,
                                             threshold=threshold)
    im_bw = cv.cvtColor(image_no_background,
                        cv.COLOR_BGR2GRAY)  # greyscale the image for single channel images: greylevel = (R+B+G)/3
    grey_level_of_color_for_cropping = sum(color_for_cropping)/len(color_for_cropping)
    im_bw[im_bw != grey_level_of_color_for_cropping] = 1
    im_bw[im_bw == grey_level_of_color_for_cropping] = 0
    holefill_filter = sitk.BinaryFillholeImageFilter()
    holefill_filter.SetForegroundValue(1)
    sitk_np = sitk.GetImageFromArray(np.ascontiguousarray(im_bw))
    filled_mask = holefill_filter.Execute(sitk_np)
    numpy_matrix_of_image = sitk.GetArrayFromImage(filled_mask)

    return (image * numpy_matrix_of_image[..., None])

def plot_keypoints(keypoints: np.array) -> None:
    for i in keypoints.keys():
        keypoints[i] = cv.cvtColor(keypoints[i], cv.COLOR_BGR2RGB)
        plt.imshow(keypoints[i])
        plt.show()
    #keypoints = cv.cvtColor(keypoints, cv.COLOR_BGR2RGB)
    #plt.imshow(keypoints)
    #plt.show()

class Image_handler:
    def __init__(self,
                 path: Path,
                 datafile_filename: str,
                 color_for_cropping: tuple):

        self.path = path
        self.image = cv.imread(str(path), cv.IMREAD_UNCHANGED)
        self.width = self.image.shape[1]
        self.height = self.image.shape[0]
        self.obtain_breast_coordinates(datafile_filename)
        self.color_for_cropping = color_for_cropping


    def plot_image(self, add_breast_contours: bool = False) -> None:
        """_summary_

        :param add_breast_contours: _description_, defaults to False
        :type add_breast_contours: bool, optional
        """
        plt.imshow(cv.cvtColor(self.image, cv.COLOR_BGR2RGB))
        if add_breast_contours:
            plt.scatter(self.left_breast_x_coordinates,
                        self.left_breast_y_coordinates,
                        s=25, c='blue', marker='o')
            plt.scatter(self.right_breast_x_coordinates,
                        self.right_breast_y_coordinates,
                        s=25, c='red', marker='o')
            plt.scatter(self.left_nipple['x'],
                        self.left_nipple['y'],
                        s=25, c='yellow', marker='o')
            plt.scatter(self.right_nipple['x'],
                        self.right_nipple['y'],
                        s=25, c='green', marker='o')
        plt.show()


    def crop_image(self) -> None:
        """_summary_
        """
        np_rgb_image = remove_background(self.color_for_cropping, self.image)
        summed_rgb_df = pd.DataFrame(np_rgb_image.sum(axis=2))
        cropped_df = summed_rgb_df.loc[summed_rgb_df[summed_rgb_df.sum(axis=1) != 0].index, summed_rgb_df.T[summed_rgb_df.T.sum(axis=1) != 0].index]
        x_cropped = np_rgb_image[cropped_df.index.values, :, :]
        fully_cropped = x_cropped[:, cropped_df.columns.values, :]

        # Generate variables
        left_crop_column = cropped_df.columns[0]
        high_crop_row = cropped_df.index[0]

        self.left_breast_x_coordinates = self.left_breast_x_coordinates - left_crop_column
        self.left_breast_y_coordinates = self.left_breast_y_coordinates - high_crop_row

        self.right_breast_x_coordinates = self.right_breast_x_coordinates - left_crop_column
        self.right_breast_y_coordinates = self.right_breast_y_coordinates - high_crop_row

        self.left_nipple["x"] = self.left_nipple["x"] - left_crop_column
        self.left_nipple["y"] = self.left_nipple["y"] - high_crop_row

        self.right_nipple["x"] = self.right_nipple["x"] - left_crop_column
        self.right_nipple["y"] = self.right_nipple["y"] - high_crop_row      

        self.width = cropped_df.columns[-1] - left_crop_column
        self.height = cropped_df.index[-1] - high_crop_row
        self.image = fully_cropped

    def resize_image(self, pixel_width: int, pixel_height: int) -> None:

        width_percentage = pixel_width / self.width
        height_percentage = pixel_height / self.height

        self.left_breast_x_coordinates = self.left_breast_x_coordinates * width_percentage
        self.right_breast_x_coordinates = self.right_breast_x_coordinates * width_percentage
        self.left_breast_y_coordinates = self.left_breast_y_coordinates * height_percentage
        self.right_breast_y_coordinates = self.right_breast_y_coordinates * height_percentage

        self.left_nipple['x'] = self.left_nipple['x'] * width_percentage
        self.right_nipple['x'] = self.right_nipple['x'] * width_percentage
        self.left_nipple['y'] = self.left_nipple['y'] * height_percentage
        self.right_nipple['y'] = self.right_nipple['y'] * height_percentage
        self.width = pixel_width
        self.height = pixel_height

        self.image = cv.resize(self.image, (pixel_width, pixel_height))

    def obtain_breast_coordinates(self, datafile_filename: str= "datafile.xml") -> None:
        df = self.read_xml_file()
        correct_paths = []
        for i in df["File Path"]:
            if i.split("/")[-1] == "{}{}".format(str(self.path.stem).lower(), ".jpg"):
                correct_paths.append(i)
        df = df[df["File Path"].isin(correct_paths)]

        #df = df[df['File Path'].str.contains(str(self.path.as_posix()).lower())]
        self.left_nipple, self.left_breast_x_coordinates, self.left_breast_y_coordinates = get_breast_information(side="Left", df=df)
        self.right_nipple, self.right_breast_x_coordinates, self.right_breast_y_coordinates = get_breast_information(side="Right", df=df)

    def generate_keypoint_patches(self, radius_weight: float = 1, shape: str = 'Circle') -> np.array:
        pixel_radius = ceil((min(self.image.shape[0:2])/12)*radius_weight)
        keypoints = {}
        
        if shape.lower() != 'square' and shape.lower() != 'circle':
            print('Non possible shape chosen! Please choose between "square" or "circle".')
            print('Shape set to default: Circle')
            shape = 'circle'
        
        
        keypoints["nipple_left_image_patch"] = self.image[int(np.rint(self.left_nipple['y']) - pixel_radius):
                                                          int(np.rint(self.left_nipple['y']) + pixel_radius),
                                                          int(np.rint(self.left_nipple['x']) - pixel_radius):
                                                          int(np.rint(self.left_nipple['x']) + pixel_radius)]
            
        keypoints["nipple_right_image_patch"]= self.image[int(np.rint(self.right_nipple['y']) - pixel_radius):
                                                          int(np.rint(self.right_nipple['y']) + pixel_radius),
                                                          int(np.rint(self.right_nipple['x']) - pixel_radius):
                                                          int(np.rint(self.right_nipple['x']) + pixel_radius)]
        
        keypoints["armpit_left_image_patch"] = self.image[int(np.rint(self.left_breast_y_coordinates['Left contour y1'].iloc[0]) - pixel_radius):
                                                          int(np.rint(self.left_breast_y_coordinates['Left contour y1'].iloc[0]) + pixel_radius),
                                                          int(np.rint(self.left_breast_x_coordinates['Left contour x1'].iloc[0]) - pixel_radius):
                                                          int(np.rint(self.left_breast_x_coordinates['Left contour x1'].iloc[0]) + pixel_radius)]
            
        keypoints["armpit_right_image_patch"] = self.image[int(np.rint(self.right_breast_y_coordinates['Right contour y1'].iloc[0]) - pixel_radius):
                                                           int(np.rint(self.right_breast_y_coordinates['Right contour y1'].iloc[0]) + pixel_radius),
                                                           int(np.rint(self.right_breast_x_coordinates['Right contour x1'].iloc[0]) - pixel_radius):
                                                           int(np.rint(self.right_breast_x_coordinates['Right contour x1'].iloc[0]) + pixel_radius)]    
        
        if shape.lower() == 'circle':
            
            new_keypoints = []
            new_image = Image.new('L', (pixel_radius*2, pixel_radius*2), 0)
            draw = ImageDraw.Draw(new_image)
            draw.pieslice(((0, 0), (pixel_radius*2, pixel_radius*2)), 0, 360, fill = 255)
            np_new_image = np.array(new_image)
            for i in keypoints.keys():
                keypoints[i] = np.dstack((keypoints[i], np_new_image))[:, :, 0:3]
        
        return keypoints
    def write_new_datafile(self, xml_filename: str) -> None:
        # Open the XML file and read it
        with open(Path(self.path.parent, 'BCCT.core.database.bct.xml'), 'r') as r:
            # Generate a new XML file with correct values
            with open(Path(self.path.parent, xml_filename), 'w') as w:
                for line in r:
                    # Check for mismatched tags
                    if 'ss:Data' in line:
                        line = line.replace('ss:Data', 'Data')
                    # Remove URL's
                    if ' xmlns="http://www.w3.org/TR/REC-html40">' in line:
                        line = line.replace(' xmlns="http://www.w3.org/TR/REC-html40"', '')
                    # Remove sup for proper names
                    if '<Sup>' in line:
                        line = line.replace('<Sup>', '')
                        line = line.replace('</Sup>', '')
                    # Remove sub for proper names
                    if '<Sub>' in line:
                        line = line.replace('<Sub>', '')
                        line = line.replace('</Sub>', '')
                    w.write(line)
                        
    def read_xml_file(self, xml_filename: str = 'datafile.xml') -> pd.DataFrame:
        
        # https://stackoverflow.com/questions/61548942/attempting-to-parse-an-xls-xml-file-using-python
        # Check if file exist, otherwise create it
        if not os.path.isfile(Path(self.path.parent, xml_filename)):
            print('Making new xml file!')
            self.write_new_datafile(xml_filename = xml_filename)
        # Namespace for the XML
        ns = {"doc": "urn:schemas-microsoft-com:office:spreadsheet"}
        # Generate Tree for XML
        tree = ET.parse(Path(self.path.parent, xml_filename))
        root = tree.getroot()
        # Append the read data in rows
        data = []
        for i, node in enumerate(root.findall('.//doc:Row', ns)):
            row = []
            for i in range(len(node)):
                row.append(get_value_of_node(node.find('doc:Cell[{}]/doc:Data'.format(i+1), ns)))
            data.append(row)
        # Change data to a DataFrame
        df = pd.DataFrame(data)
        df = pd.DataFrame(df.values[1:], columns=df.iloc[0])   
        # Change values to float for columns without strings
        for column in df:
            if 'path' in str(column).lower() or 'date' in str(column).lower() or 'software' in str(column).lower():
                pass
            else:
                df[column] = df[column].astype(float)
        # Rename 'Left nipple x' duplicate
        if df['Left nipple x'].iloc[0][0] < df['Left nipple x'].iloc[0][1]:
            rename_dict = {'Left nipple x': ['Left nipple x', 'Right nipple x']}
        else:
            rename_dict = {'Left nipple x': ['Right nipple x', 'Left nipple x']}
        df = df.rename(columns=lambda c: rename_dict[c].pop(0) if c in rename_dict.keys() else c)
        # Return the DataFrame
        return df


# %%

if __name__ == "__main__":
    make_the_big_run = False
    save_directory = "E:\\Jasper\\BCCTCore2.0\\temporary_folder"
    from_directory = "E:\\Jasper\\DBCGRT"
    hospitals = ["Aalborg", "Aarhus", "Dresden CGC", "Odense", "Vejle"]
    localxml_db_filename = 'BCCT.core.database.bct.xml'

    if make_the_big_run:
        cnt = 0
        percenter= 0
        collected_df = []
        errors = []
        dfs = []
        for hospital in hospitals:
            bool_vector= []
            hospital_data_path = "{}\\{}\\BCCT HYPO {} 03.2018".format(from_directory, hospital, hospital)
            if ((hospital == "Odense") | (hospital == "Aarhus") | (hospital == "Vejle")):
                filepath = "{} 2.xlsx".format(hospital_data_path)
            else: 
                filepath = "{}.xlsx".format(hospital_data_path)
            xl_file = pd.read_excel(filepath)
            df_with_path = xl_file[~xl_file["repeated path"].isna()]
            for i in df_with_path.index:
                abs_path = "{}\\{}\\{}".format(hospital_data_path, df_with_path['Patient folder'].loc[i], df_with_path["repeated path"].loc[i].split('/')[-1])
                if os.path.exists(abs_path):
                    df_with_path.loc[i, "repeated path"] = abs_path
                    bool_vector.append(i)
                else:
                    decoded_path = manipulate_string(bytes(df_with_path["repeated path"].loc[i],'utf-8').decode("ANSI"), {U"\u00AE": ".", U"\u00B2": "r", U"\u00A0": ' ', U"\u00A3": 'c', 
                                                                                                                        U"\u00AD": 'm', U"\u00A9": ')', U"\u00B3": "s", U"\u00A8": "(",
                                                                                                                        U"\u00AC": 'l', U"\u00a5": 'e', ".°": 'np', "årm": "år-", 
                                                                                                                        "/fr ": "/før ", " fr ": " før ", U"\u00a1": "A", U"\u00b4": "t",
                                                                                                                        U"\u00AA": 'j', "årl": 'år,', ".¤": 'nd', U"\u00AB": 'k',
                                                                                                                        U"\u00A2": 'b', U"\u00A7": '\'', "1岯": "/", "Ã¸": 'ø', "Ãe": 'å'})
                    if ((hospital == "Aalborg") | (hospital == "Aarhus")): 
                        abs_path = "{}\\{}\\{}".format(hospital_data_path, df_with_path['Patient folder'].loc[i], decoded_path.split('/')[-1])
                    else:
                        abs_path = "{}{}".format(hospital_data_path, decoded_path.split("BCCT HYPO {} 03.2018".format(hospital).lower().replace("/", "\\"))[-1].replace("/", "\\"))
                    if os.path.exists(abs_path):
                        df_with_path.loc[i, "repeated path"] = abs_path
                        bool_vector.append(i)
                    else:
                        if "  " in abs_path:
                            if os.path.exists(abs_path.replace("  ", " ")):
                                df_with_path.loc[i, "repeated path"] = abs_path.replace("  ", " ")
                                bool_vector.append(i)
                            elif os.path.exists(abs_path.replace("  ", "")):
                                df_with_path.loc[i, "repeated path"] = abs_path.replace("  ", "")
                                bool_vector.append(i)
                            else:
                                errors.append(abs_path)
                        elif os.path.exists(abs_path.replace(abs_path.split("\\")[-1][0:len(abs_path.split("\\")[-2])-1],  abs_path.split("\\")[-2][0:len(abs_path.split("\\")[-2])])):
                            df_with_path.loc[i, "repeated path"] = abs_path.replace(abs_path.split("\\")[-1][0:len(abs_path.split("\\")[-2])-1],  abs_path.split("\\")[-2][0:len(abs_path.split("\\")[-2])])
                            bool_vector.append(i)
                        elif os.path.exists(abs_path.replace(abs_path.split("\\")[-1][0:3],  (abs_path.split("\\")[-2][0:3] + abs_path.split("\\")[-1][4]))):
                            df_with_path.loc[i, "repeated path"] = abs_path.replace(abs_path.split("\\")[-1][0:3],  (abs_path.split("\\")[-2][0:3] + abs_path.split("\\")[-1][4]))
                            bool_vector.append(i)
                        else:
                            errors.append(abs_path)
            dfs.append(df_with_path.loc[bool_vector]) #End of admin file handling

            for path, subdirs, files in os.walk(hospital_data_path):
                for name in files:
                    if name.lower() == localxml_db_filename.lower():
                        df = pd.DataFrame()                                
                        filepath = os.path.join(path, name)
                        # print(filepath)
                        with open(filepath, "r") as file:
                            # Read each line in the file, readlines() returns a list of lines
                            content = file.readlines()
                            # Combine the lines in the list into a string
                            content = "".join(content)
                            bs_content = bs(content, "lxml")

                            for worksheet in bs_content.find_all('worksheet'):
                                rows = worksheet.find_all('row')
                                # header_row = worksheet.select('table > row')[0]
                                header_row = rows[0]
                                headers = header_row.find_all('cell')
                                #find the cell which contains the 'repeated path' tag
                                #header = header_row.find('cell', text='repeated path')
                                
                                for i in range(1,len(rows)):
                                    for header in headers:
                                        dataline = rows[i]
                                        data = dataline.find_all('cell')
                                        txt = data[headers.index(header)].text.strip()
                                        if header.text.strip() == "repeated path": 
                                            folder = os.path.basename(os.path.dirname(txt))
                                        
                                            filestr = os.path.join(os.path.dirname(filepath), os.path.basename(txt))
                                            if not(os.path.exists(filestr)):
                                            #    print(filestr)
                                                corruptfilename = os.path.basename(txt)
                                                #replace the superscript 2 with a dot 
                                                corruptfilename = corruptfilename.replace(u"\u00AE", '.')
                                                #replace the superscript 2 with a r                             
                                                corruptfilename = corruptfilename.replace(u"\u00B2", 'r')
                                                #replace the non gap space with a full space
                                                corruptfilename = corruptfilename.replace(u"\u00A0", ' ')
                                                filestr = os.path.join(os.path.dirname(filepath),corruptfilename)
                                            #print(filestr, os.path.exists(filestr))
                                            if os.path.exists(filestr):
                                                df.loc[i, header.text.strip()] = filestr
                                            else:
                                                df.loc[i, header.text.strip()] = np.NaN
                                        else:
                                            df.loc[i, header.text.strip()] = data[headers.index(header)].text.strip()
                                    cnt += 1
                                    if cnt%84 == 0:
                                        percenter +=1
                                        print("{}% {}".format(percenter, "finished"))
                        collected_df.append(df)


        for i in range(len(errors)):
            print(errors[i].split("\\")[-2])
        df_administration_files = pd.concat(dfs, axis=0)
        df_administration_files["repeated path"] = df_administration_files["repeated path"].str.rstrip().str.lower() 
        collected_bcct_core_data = pd.concat(collected_df).reset_index(drop=True)
        collected_bcct_core_data["repeated path"] = collected_bcct_core_data["repeated path"].str.rstrip().str.lower() 
        suffix = "from_admin_file" 
        merged = collected_bcct_core_data.merge(df_administration_files, on="repeated path", suffixes=('', suffix))
        merged = merged.drop(columns=(collected_bcct_core_data.columns.drop(["Overall classification", "repeated path"]) + suffix)) #Add to this list if more data from admin file is required. 
        pd.to_pickle(merged, save_directory+ "\\merged_bcct_core_and_admin_files.pkl")
    else:
        merged = pd.read_pickle(save_directory+"\\merged_bcct_core_and_admin_files.pkl")

    new_file_path = "E:\\Jasper\\BCCTCore2.0"

    for index, row in merged.iterrows():
        print(index)
        temp_img = cv.imread(row["repeated path"], cv.IMREAD_UNCHANGED)
        directory = Path(new_file_path, row["Institution"])
        os.chdir(directory)
        name = str(row["Randomization number"]) + " year " + str(row["Year"])
        cv.imwrite(name + ".png", temp_img)
        row.to_json(Path(directory, name + '.json'))



    print("Done")

    for i in range(2):
        roots = []
        roots.append(os.path.join(dirname, 'Jasper', 'DBCGRT', 'Aalborg', 'BCCT HYPO Aalborg 03.2018'))
        roots.append(os.path.join(dirname, 'Jasper', 'DBCGRT', 'Aarhus', 'BCCT HYPO Aarhus 03.2018'))
        roots.append(os.path.join(dirname, 'Jasper', 'DBCGRT', 'Dresden CGC', 'BCCT HYPO Dresden CGC 03.2018'))
        roots.append(os.path.join(dirname, 'Jasper', 'DBCGRT', 'Odense', 'BCCT HYPO Odense 03.2018'))
        roots.append(os.path.join(dirname, 'Jasper', 'DBCGRT', 'Vejle', 'BCCT HYPO Vejle 03.2018'))


        filename = 'BCCT.core.database.bct.xml'
        errors = []
        correct_paths = []
        cnt_true = 0
        for root in roots:
            for path, subdirs, files in os.walk(root):
                for name in files:
                    if name.lower() == filename.lower():
                        df = pd.DataFrame()                                
                        filepath = os.path.join(path, name)
                        # print(filepath)
                        with open(filepath, "r") as file:
                            # Read each line in the file, readlines() returns a list of lines
                            content = file.readlines()
                            # Combine the lines in the list into a string
                            content = "".join(content)
                            bs_content = bs(content, "lxml")

                            for worksheet in bs_content.find_all('worksheet'):
                                rows = worksheet.find_all('row')
                                # header_row = worksheet.select('table > row')[0]
                                header_row = rows[0]
                                headers = header_row.find_all('cell')
                                #find the cell which contains the 'repeated path' tag
                                #header = header_row.find('cell', text='repeated path')
                                
                                for i in range(1,len(rows)):
                                    for header in headers:
                                        dataline = rows[i]
                                        data = dataline.find_all('cell')
                                        txt = data[headers.index(header)].text.strip()
                                        if header.text.strip() == "repeated path": 
                                            folder = os.path.basename(os.path.dirname(txt))
                                        
                                            filestr = os.path.join(os.path.dirname(filepath), os.path.basename(txt))
                                            if not(os.path.exists(filestr)):
                                            #    print(filestr)
                                                corruptfilename = os.path.basename(txt)
                                                #replace the superscript 2 with a dot 
                                                corruptfilename = corruptfilename.replace(u"\u00AE", '.')
                                                #replace the superscript 2 with a r                             
                                                corruptfilename = corruptfilename.replace(u"\u00B2", 'r')
                                                #replace the non gap space with a full space
                                                corruptfilename = corruptfilename.replace(u"\u00A0", ' ')
                                                filestr = os.path.join(os.path.dirname(filepath),corruptfilename)
                                            #print(filestr, os.path.exists(filestr))
                                            if os.path.exists(filestr):
                                                cnt_true +=1
                                                #correct_paths.append(filestr)
                                                df.loc[i, header.text.strip()] = filestr
                                            else:
                                                #errors.append(filestr)
                                                df.loc[i, header.text.strip()] = np.NaN
                                        else:
                                            df.loc[i, header.text.strip()] = data[headers.index(header)].text.strip()
                                    cnt += 1
                                    if cnt%100 == 0:
                                        print("wuhuu 100 patients more")
                        collected_df.append(df)

        collected_bcct_core_data = pd.concat(collected_df).reset_index(drop=True)
        collected_bcct_core_data["repeated path"] = collected_bcct_core_data["repeated path"].str.rstrip().str.lower() 
        suffix = "from_admin_file" 
        merged = collected_bcct_core_data.merge(df_administration_files, on="repeated path", suffixes=('', suffix))
        merged = merged.drop(columns=(collected_bcct_core_data.columns.drop(["Overall classification", "repeated path"]) + suffix)) #Add to this list if more data from admin file is required. 
        pd.to_pickle(merged, save_directory+ "\\merged_bcct_core_and_admin_files.pkl")

    #else:
     #    merged = pd.read_pickle(save_directory+"\\merged_bcct_core_and_admin_files.pkl")


    for folder in os.listdir(photo_folder):
        for filename in os.listdir("{}/{}".format(photo_folder, folder)):
            if frontfacing_key in filename:
                img_path = Path(photo_folder, filename)
                test = Image_handler(path=img_path, datafile_filename=datafile_name, color_for_cropping=white)
                test.plot_image()
                percentage = 0.2
                test.resize_image(int(test.width * percentage), int(test.height * percentage))
                test.plot_image()
                keypoints = test.generate_keypoint_patches(radius_weight=0.5)
                collected_keypoints[filename] = keypoints
                nipple_l.append(cv.cvtColor(keypoints["nipple_left_image_patch"], cv.COLOR_BGR2RGB))
                nipple_r.append(cv.cvtColor(keypoints["nipple_right_image_patch"], cv.COLOR_BGR2RGB))
                armpit_l.append(cv.cvtColor(keypoints["armpit_left_image_patch"], cv.COLOR_BGR2RGB))
                armpit_r.append(cv.cvtColor(keypoints["armpit_right_image_patch"], cv.COLOR_BGR2RGB))
                naming.append(filename)
                #error_list = validate_keypoints(keypoints)
                plot_keypoints(keypoints)
                #images.append(test)
                #plt.show()
                test.crop_image()
                percentage = 0.1
                #test.resize_image(int(test.width * percentage), int(test.height * percentage))
                test.plot_image(add_breast_contours=True)

    
    fig = go.Figure(
        data=[
            go.Bar(
                x=np.linalg.norm((pca_matrix-centroid), axis=1),
                text=naming, #text is distance from centroid 
            )
        ]
    )
    plot(fig)
    print("af")
            
            
        

    
