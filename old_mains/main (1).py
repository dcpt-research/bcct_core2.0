# -*- coding: utf-8 -*-
"""
Created on Mon Aug 22 08:15:45 2022
@authors: Morten Sahlertz & Jacob Kjærager
"""

import os
import cv2 as cv
import numpy as np
import pandas as pd
from PIL import Image
import SimpleITK as sitk
from pathlib import Path
from matplotlib import patches
import matplotlib.pyplot as plt
from plotly.offline import plot
import plotly.graph_objects as go
from sklearn.decomposition import PCA
from dataimport import import_data, import_tempdata
from cvzone.SelfiSegmentationModule import SelfiSegmentation


class Image_handler:
    def __init__(self,
                 path: Path,
                 color_for_cropping: tuple):
        
        self.path = path
        self.image = cv.imread(str(path), cv.IMREAD_UNCHANGED)
        self.width = self.image.shape[1]
        self.height = self.image.shape[0]
        
        self.obtain_breast_coordinates()

        self.color_for_cropping = color_for_cropping
        
    def plot_image_with_contours(self):
        
        plt.imshow(cv.cvtColor(self.image, cv.COLOR_BGR2RGB))
        
        plt.scatter(self.left_breast_x_coordinates,
                    self.left_breast_y_coordinates,
                    s=25, c='blue', marker='o')
        plt.scatter(self.right_breast_x_coordinates,
                    self.right_breast_y_coordinates,
                    s=25, c='red', marker='o')
        plt.scatter(self.left_nipple['x'],
                    self.left_nipple['y'],
                    s=25, c='yellow', marker='o')
        plt.scatter(self.right_nipple['x'],
                    self.right_nipple['y'],
                    s=25, c='green', marker='o')
        plt.show()
        
    def plot_image(self):
        
        plt.imshow(cv.cvtColor(self.image, cv.COLOR_BGR2RGB))
        
        plt.show()
        
    def remove_background(self, threshold: float = 0.4) -> np.array:
    
        segmentor = SelfiSegmentation()
        image_no_background = segmentor.removeBG(self.image,
                                                 imgBg=self.color_for_cropping,
                                                 threshold=threshold)
        im_bw = cv.cvtColor(image_no_background,
                            cv.COLOR_BGR2GRAY)  # greyscale the image for single channel images: greylevel = (R+B+G)/3
        im_bw[im_bw <= 0] = 0
        im_bw[im_bw > 0] = 1
        holefill_filter = sitk.BinaryFillholeImageFilter()
        holefill_filter.SetForegroundValue(1)
        sitk_np = sitk.GetImageFromArray(np.ascontiguousarray(im_bw))
        filled_mask = holefill_filter.Execute(sitk_np)
        numpy_matrix_of_image = sitk.GetArrayFromImage(filled_mask)
        return image_no_background
    
    def crop_image(self):
        
        image_no_background = self.remove_background()
        # Generate variables
        pixel_count_l = 0
        pixel_count_r = 0
        # Run through the image from the right hand side
        for i in range(image_no_background.shape[1]):
            if np.all(image_no_background[:, i] == self.color_for_cropping):
                pixel_count_r += 1
            else:
                break
        # Run through the image from the left hand side
        for i in range(image_no_background.shape[1]-1, 0, -1):
            if np.all(image_no_background[:, i] == self.color_for_cropping):
                pixel_count_l += 1
            else:
                break
        # Change x coordinates and image
        self.left_breast_x_coordinates = self.left_breast_x_coordinates - pixel_count_r
        self.right_breast_x_coordinates = self.right_breast_x_coordinates - pixel_count_r
        self.left_nipple['x'] = self.left_nipple['x'] - pixel_count_r
        self.right_nipple['x'] = self.right_nipple['x'] - pixel_count_r
        self.width = self.width - (pixel_count_l + pixel_count_r)
        self.image = self.image[:, pixel_count_r:-pixel_count_l]
        
    def resize_image(self, pixel_width: int, pixel_height: int):
        
        width_percentage = pixel_width / self.width
        height_percentage = pixel_height / self.height
        
        self.left_breast_x_coordinates = self.left_breast_x_coordinates * width_percentage
        self.right_breast_x_coordinates = self.right_breast_x_coordinates * width_percentage
        self.left_breast_y_coordinates = self.left_breast_y_coordinates * height_percentage
        self.right_breast_y_coordinates = self.right_breast_y_coordinates * height_percentage
        
        self.left_nipple['x'] = self.left_nipple['x'] * width_percentage
        self.right_nipple['x'] = self.right_nipple['x'] * width_percentage
        self.left_nipple['y'] = self.left_nipple['y'] * height_percentage
        self.right_nipple['y'] = self.right_nipple['y'] * height_percentage
        
        self.width = pixel_width
        self.height = pixel_height
        
        self.image = cv.resize(self.image, (pixel_width, pixel_height))        
    
    def obtain_breast_coordinates(self):
        
        df = pd.read_table(Path(self.path.parent, 'datafile.csv'),
                           delimiter=";",
                           decimal=",")
        df = df[df['File Path'].str.contains(str(self.path.as_posix()).lower())]
        
        breast_contour_coordinates = df[df.columns[df.columns.str.contains(side)]]
        nipple_coordinates = df[breast_contour_coordinates.columns[breast_contour_coordinates.columns.str.contains("nipple")]]
        self.left_nipple = {"x": nipple_coordinates["{} nipple x".format(side)],
                            "y": nipple_coordinates["{} nipple y".format(side)]}
        self.left_breast_x_coordinates = df[breast_contour_coordinates.columns[breast_contour_coordinates.columns.str.contains("x")]].drop(columns=['{} nipple x'.format(side)])
        self.left_breast_y_coordinates = df[breast_contour_coordinates.columns[breast_contour_coordinates.columns.str.contains("y")]].drop(columns=['{} nipple y'.format(side)])
        
        right_group = df[df.columns[df.columns.str.contains("Right")]]
        nipple_right = df[right_group.columns[right_group.columns.str.contains("nipple")]]
        self.right_nipple = {"x": nipple_right["Right nipple x"],
                             "y": nipple_right["Right nipple y"]}
        self.right_breast_x_coordinates = df[right_group.columns[right_group.columns.str.contains("x")]].drop(columns=['Right nipple x'])
        self.right_breast_y_coordinates = df[right_group.columns[right_group.columns.str.contains("y")]].drop(columns=['Right nipple y'])
        


# def remove_background(image, color, threshold):
    
#     # image = cv.imread(image,cv.IMREAD_UNCHANGED)
#     segmentor = SelfiSegmentation()
#     image_no_background = segmentor.removeBG(image,
#                                              imgBg=color,
#                                              threshold=threshold)
    
#     return image_no_background

# def crop_image(image, image_no_background, color) -> np.array:
#     # Generate variables
#     pixel_count_l = 0
#     pixel_count_r = 0
#     # Run through the image from the right hand side
#     for i in range(image_no_background.shape[1]):
#         if np.all(image_no_background[:, i] == color):
#             pixel_count_r += 1
#         else:
#             break
#     # Run through the image from the left hand side
#     for i in range(image_no_background.shape[1]-1, 0, -1):
#         if np.all(image_no_background[:, i] == color):
#             pixel_count_l += 1
#         else:
#             break
#     # Return image
#     return image[:, pixel_count_r:-pixel_count_l]

# def resize_image(image: np.array,
#                  pixel_width: int,
#                  pixel_height: int,
#                  left_group_x,
#                  left_group_y,
#                  right_group_x,
#                  right_group_y):
    
#     width_percentage = pixel_width / image.shape[1] 
#     height_percentage = pixel_height / image.shape[0]
    
#     left_group_x = left_group_x * width_percentage
#     right_group_x = right_group_x * width_percentage
#     left_group_y = left_group_y * height_percentage
#     right_group_y = right_group_y * height_percentage
    
#     image_resized = cv.resize(image, (pixel_width, pixel_height))
    
#     return image_resized, left_group_x, left_group_y, right_group_x, right_group_y

#%%

if __name__ == "__main__":
    # %%

    images = []

    white = (255, 255, 255)

    test = Image_handler(Path("420001 SMR", "420001 091113 arme ned.jpg"), white)
    test.plot_image()
    images.append(test)
    plt.show()
    test.crop_image()
    percentage = 0.2
    test.resize_image(int(test.width * percentage), int(test.height * percentage))

    test.plot_image_with_contours()
    print("afew")

    # %%




    bb_height = 300
    bb_width = 300
    df = pd.read_table("./420001 SMR/datafile.csv", delimiter=";", decimal=",")
    left_group = df[df.columns[df.columns.str.contains("Left")]]
    all_nipple_left = df[left_group.columns[left_group.columns.str.contains("nipple")]]
    left_group_x_coordinate = df[left_group.columns[left_group.columns.str.contains("x")]]
    left_group_y_coordinate = df[left_group.columns[left_group.columns.str.contains("y")]]
    right_group = df[df.columns[df.columns.str.contains("Right")]]
    all_nipple_right = df[right_group.columns[right_group.columns.str.contains("nipple")]]
    right_group_x_coordinate = df[right_group.columns[right_group.columns.str.contains("x")]]
    right_group_y_coordinate = df[right_group.columns[right_group.columns.str.contains("y")]]
    armpit_le_red = []
    armpit_le_green = []
    armpit_le_blue = []
    armpit_ri_red = []
    armpit_ri_green = []
    armpit_ri_blue = []
    pca = PCA(n_components=3)
    img_text = []
    img_index = 0
    #for img_index in range(0, 6):
    img_text.append(img_index)
    test_file = "./420001 SMR/{}".format([filename for filename in os.listdir("./420001 SMR") if "arme ned" in filename][img_index])
    img = Image.open(test_file)
    
    nipple_right = {"x":all_nipple_right["Right nipple x"][img_index],
                    "y":all_nipple_right["Right nipple y"][img_index]}
    nipple_left = {"x": all_nipple_left["Left nipple x"][img_index],
                   "y" :all_nipple_left["Left nipple y"][img_index]}
    armpit_right = {"x": right_group_x_coordinate['Right contour x1'][img_index],
                    "y":right_group_y_coordinate['Right contour y1'][img_index]}
    armpit_left = {"x": left_group_x_coordinate['Left contour x1'][img_index],
                   "y":left_group_y_coordinate['Left contour y1'][img_index]}

    nipple_left_patch = img.crop((nipple_left["x"]-bb_width, nipple_left["y"]-bb_height,
                         nipple_left["x"]+bb_width, nipple_left["y"]+bb_height))
    nipple_right_patch = img.crop((nipple_right["x"]-bb_width, nipple_right["y"]-bb_height,
                         nipple_right["x"]+bb_width, nipple_right["y"]+bb_height))
    armpit_left_patch = img.crop((armpit_left["x"]-bb_width, armpit_left["y"]-bb_height,
                         armpit_left["x"]+bb_width, armpit_left["y"]+bb_height))
    armpit_right_patch = img.crop((armpit_right["x"]-bb_width, armpit_right["y"]-bb_height,
                         armpit_right["x"]+bb_width, armpit_right["y"]+bb_height))
    red1, green1, blue1 = armpit_left_patch.split()
    red2, green2, blue2 = armpit_right_patch.split()

    armpit_le_red.append(pd.DataFrame(np.asarray(red1)/255))
    armpit_le_green.append(pd.DataFrame(np.asarray(green1)/255)[0])
    armpit_le_blue.append(pd.DataFrame(np.asarray(blue1)/255)[0])

    armpit_ri_red.append(pd.DataFrame(np.asarray(red2)/255)[0])
    armpit_ri_green.append(pd.DataFrame(np.asarray(green2)/255)[0])
    armpit_ri_blue.append(pd.DataFrame(np.asarray(blue2)/255)[0])
    plt.imshow(nipple_left_patch)
    fig1, ax1 = plt.subplots()
    ax1.imshow(nipple_right_patch)
    fig2, ax2 = plt.subplots()
    ax2.imshow(armpit_left_patch)
    fig3, ax3 = plt.subplots()
    ax3.imshow(armpit_right_patch)
    fig4, ax4 = plt.subplots()
    plt.plot([armpit_right["x"], armpit_left["x"]], [armpit_right["y"], armpit_left["y"]], color="white", linewidth=3)
    print("width between armpits: {}pixels".format(armpit_right["x"] - armpit_left["x"]))
    ax4.imshow(img)
    # # Create a Rectangle patch
    rect_right_nipple = patches.Rectangle((nipple_right["x"]-(bb_width/2),
                                           nipple_right["y"]-(bb_height/2)),
                                           bb_height, bb_width, linewidth=1, edgecolor='r', facecolor='none')
    ax2.add_patch(rect_right_nipple)
    rect_left_nipple = patches.Rectangle((nipple_left["x"] - (bb_width / 2),
                                          nipple_left["y"] - (bb_height / 2)),
                                          bb_height, bb_width, linewidth=1, edgecolor='r', facecolor='none')
    ax2.add_patch(rect_left_nipple)
    rect_right_armpit = patches.Rectangle((armpit_right['x']-(bb_width/2),
                                           armpit_right['y']-(bb_height/2)),
                                           bb_height, bb_width, linewidth=1, edgecolor='r', facecolor='none')
    ax2.add_patch(rect_right_armpit)
    rect_left_armpit = patches.Rectangle((armpit_left['x']-(bb_width/2),
                                          armpit_left['y']-(bb_height/2)),
                                          bb_height, bb_width, linewidth=1, edgecolor='r', facecolor='none')
    ax2.add_patch(rect_left_armpit)
    #
    plt.show()
    plt.imshow(img)
    plt.scatter(right_group_x_coordinate.iloc[img_index],
                right_group_y_coordinate.iloc[img_index],
                s=25, c='red', marker='o')
    plt.scatter(left_group_x_coordinate.iloc[img_index],
                left_group_y_coordinate.iloc[img_index],
                s=25, c='blue', marker='o')
    plt.scatter(all_nipple_right["Right nipple x"][img_index],
                all_nipple_right["Right nipple y"][img_index],
                s=25, c='yellow', marker='o')
    plt.scatter(all_nipple_left["Left nipple x"][img_index],
                all_nipple_left["Left nipple y"][img_index],
                s=25, c='green', marker='o')
    print("awef")
    plt.show()
    print("aewf")
    

    
#%% 

    images = []

    white = (255, 255, 255)

    test = Image_handler(Path("420001 SMR","420001 091113 arme ned.jpg"), white)
    test.plot_image()
    images.append(test)
    plt.show()
    test.crop_image()
    percentage = 0.2
    test.resize_image(int(test.width * percentage), int(test.height * percentage))

    test.plot_image_with_contours()
    print("afew")

#%%

import xml.etree.ElementTree as ET
import pandas as pd

xml_data = open("420001 SMR/BCCT.core.database.bct.xml", 'r').read()  # Read file
root = ET.XML(xml_data)  # Parse XML

data = []
cols = []
for i, child in enumerate(root):
    data.append([subchild.text for subchild in child])
    cols.append(child.tag)


#%%

df1 = pd.read_xml("BCCT.core.database.bct.xml")
df2 = pd.read_excel("BCCT.core.database.bct.xml")
df_csv = df1.to_csv()
# df = pd.DataFrame(data).T  # Write in DF and transpose it
# df.columns = cols  # Update column names
# print(df)

# xml_data = open("420001 SMR/BCCT.core.database.bct.xml", 'r').read()  # Read file

# test_xml = pd.read_xml(xml_data)
    
    # import numpy as np
    # import cv2
    # import matplotlib.pyplot as plt
    #
    # import torch
    # from torchvision.models.segmentation import deeplabv3_resnet101
    # from torchvision import transforms
    #
    #
    # def make_deeplab(device):
    #     deeplab = deeplabv3_resnet101(pretrained=True).to(device)
    #     deeplab.eval()
    #     return deeplab
    #
    #
    # device = torch.device("cpu")
    # deeplab = make_deeplab(device)
    #
    # img_orig = cv2.imread(test_file, 1)
    # plt.imshow(img_orig[:, :, ::-1])
    # plt.show()
    #
    # k = min(1.0, 1024 / max(img_orig.shape[0], img_orig.shape[1]))
    # img = cv2.resize(img_orig, None, fx=k, fy=k, interpolation=cv2.INTER_LANCZOS4)
    # deeplab_preprocess = transforms.Compose([
    #     transforms.ToTensor(),
    #     transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
    # ])
    # def apply_deeplab(deeplab, img, device):
    #     input_tensor = deeplab_preprocess(img)
    #     input_batch = input_tensor.unsqueeze(0)
    #     with torch.no_grad():
    #         output = deeplab(input_batch.to(device))['out'][0]
    #     output_predictions = output.argmax(0).cpu().numpy()
    #     return (output_predictions == 15)
    #
    #
    # mask = apply_deeplab(deeplab, img, device)
    #
    # plt.imshow(mask, cmap="gray")
    # plt.show()
    #
    #
    # fig = go.Figure(
    #     data= [
    #         go.Scatter3d(
    #             x=pd.DataFrame(armpit_ri_red)[0],
    #             y=pd.DataFrame(armpit_ri_red)[1],
    #             z=pd.DataFrame(armpit_ri_red)[2],
    #             mode='markers',
    #             text=img_text,
    #             marker=dict(
    #                 color="red"
    #             )
    #         ),
    #         go.Scatter3d(
    #             x=pd.DataFrame(armpit_ri_green)[0],
    #             y=pd.DataFrame(armpit_ri_green)[1],
    #             z=pd.DataFrame(armpit_ri_green)[2],
    #             mode='markers',
    #             text=img_text,
    #             marker=dict(
    #                 color="green"
    #             )
    #         ),
    #         go.Scatter3d(
    #             x=pd.DataFrame(armpit_ri_blue)[0],
    #             y=pd.DataFrame(armpit_ri_blue)[1],
    #             z=pd.DataFrame(armpit_ri_blue)[2],
    #             mode='markers',
    #             text=img_text,
    #             marker=dict(
    #                 color="blue"
    #             )
    #         ),
    #     ]
    # )
    #
    # plot(fig)
    # print("afew")
    # site = "Site 1" #Must be part of filename for files of interest.
    # new_data_import = True
    # add_weatherdata = False

    # if add_weatherdata:
    #     weather_suffix = "_with_weatherdata"
    # else:
    #     weather_suffix = "_without_weatherdata"
    # if new_data_import:
    #     VOI = 'Temperature_sensor'
    #     df_site = import_data(import_set=site)
    #     df_site = generate_one_minute_avg(df=df_site, VOI=VOI)
    #     df_site = calculate_below_avg_samples(df=df_site,
    #                                           VOI=VOI,
    #                                           below=True,
    #                                           smallest=True)
    #     df_site = calculate_below_avg_samples(df=df_site,
    #                                           VOI=VOI,
    #                                           below=False,
    #                                           smallest=False)
    #     df_site = get_max_h2s_within_temp_dips(df=df_site)
    #     #df_site = map_cyclical_features(df=df_site) #Used to map cyclical features to sin/cos values for better ML
    #     if add_weatherdata:
    #         df_site = import_tempdata(df=df_site, keep_columns=["Temperatur", 'Regn - minut', 'Date (UTC)'],
    #                                   filename="Silkeborg_vejr_data")
    #         df_site = df_site.rename(columns={"Temperatur": "Weather_temperature", "Regn - minut": "Weather_rain"})
    #     df_site.to_pickle("{}".format(Path("../", "large_files", "pickle_files", "df_{}_{}.pkl".format(site, weather_suffix))))
    # else:
    #     df_site = pd.read_pickle(Path("../", "large_files", "pickle_files", "df_{}_{}.pkl".format(site, weather_suffix)))
    # plot_temp_and_H2S(data=df_site, enable_weather_data=add_weatherdata)

#%%
    
    # img = cv.imread("./420001 SMR/420001 101217 arme op.jpg")
    # # img = cv.imread("./420001 SMR/420001 091113 arme ned.jpg")
    
    # white = (255, 255, 255)
    # black = (0, 0, 0)
    
    # img_no_bg = remove_background(img, color=black, threshold=0.6)
    
    # img_cropped = crop_image(img, img_no_bg, color=black)
    
    # percentage = 0.2
    # img_cropped = cv.resize(img_cropped,
    #                         (int(img_cropped.shape[1]*percentage),
    #                           int(img_cropped.shape[0]*percentage)))
    
    # # img_cropped, left_group_x_coordinate, left_group_y_coordinate, right_group_x_coordinate, right_group_y_coordinate = resize_image(img_cropped,
    # #                                                                                                                                 int(img_cropped.shape[1]*percentage),
    # #                                                                                                                                 int(img_cropped.shape[0]*percentage),
    # #                                                                                                                                 left_group_x_coordinate,
    # #                                                                                                                                 left_group_y_coordinate,
    # #                                                                                                                                 right_group_x_coordinate,
    # #                                                                                                                                 right_group_y_coordinate)
                               
    
    # img = cv.resize(img,
    #                 (int(img.shape[1]*percentage),
    #                   int(img.shape[0]*percentage)))
    
    
    
    
    # cv.imshow("Cropped Image", img_cropped)
    # cv.imshow("Image", img)
    
    # cv.waitKey(0)
    # cv.destroyAllWindows()
    
    
    #%%
    
    
    # img = cv.imread("./420001 SMR/420001 091113 arme ned.jpg")
    
    # percentage = 0.2
    
    # img, left_group_x, left_group_y, right_group_x, right_group_y = resize_image(img,
    #                                                                              int(img.shape[1]*percentage),
    #                                                                              int(img.shape[0]*percentage),
    #                                                                              left_group_x_coordinate,
    #                                                                              left_group_y_coordinate,
    #                                                                              right_group_x_coordinate,
    #                                                                              right_group_y_coordinate)
    
    # plt.imshow(img)
    # plt.scatter(right_group_x.iloc[img_index],
    #             right_group_y.iloc[img_index],
    #             s=25, c='red', marker='o')
    # plt.scatter(left_group_x.iloc[img_index],
    #             left_group_y.iloc[img_index],
    #             s=25, c='blue', marker='o')
    # plt.show()