from typing import List, Union
from venv import create
import pandas as pd
import os
import numpy as np
import math
from pathlib import Path
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from sklearn.utils.class_weight import compute_class_weight
from keras_preprocessing.image import ImageDataGenerator
from tensorflow.keras.applications import EfficientNetB0, EfficientNetB7
from tensorflow.keras.callbacks import ReduceLROnPlateau
from tensorflow.keras.models import Model
from operator import itemgetter 
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.layers import Dense, GlobalAveragePooling2D, Dropout
from sklearn.model_selection import KFold

def construct_set(keys: list, group: pd.core.groupby.generic.DataFrameGroupBy) -> pd.DataFrame:
    to_concat = []
    for key in keys:
        to_concat.append(group.get_group(key))
    return pd.concat(to_concat)

def get_trainingsets(hospitals: list) -> Union[pd.DataFrame, pd.DataFrame]:
    
    individual_trainsets = []
    individual_testsets = []
    for  hospital in hospitals: 
        # Generate groups                              
        grouped = df[df['Institution'] == hospital].groupby('Randomization number')
        # Generate train data
        train, test = train_test_split(list(grouped.groups.keys()), test_size=0.3)

        train_set = construct_set(train, grouped)
        test_set = construct_set(test, grouped)
        individual_trainsets.append(train_set)
        individual_testsets.append(test_set)
    return [pd.concat(individual_trainsets), pd.concat(individual_testsets)]

if __name__ == "__main__":
    #os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'
    os.environ['CUDA_VISIBLE_DEVICES'] = '1'

    print(os.getcwd())
    base_path = Path(os.getcwd())
    df = pd.read_pickle(Path(base_path, "merged_file_for_testing.pkl"))
    df["new_abs_path"] = Path(base_path.parent, "testing_new").as_posix() + "//" + df['Randomization number'].astype(str) + " year " + df['Year'].astype(str)+ ".png"
    df["binary_score"] = df["binary_score"].astype(str)
    train_datagen=ImageDataGenerator(rescale=1./255,
                                     width_shift_range=[-5,5], 
                                     height_shift_range=0.05, 
                                     vertical_flip=True, 
                                     brightness_range=[0.75,1.25],
                                     zoom_range=[0.95,1.05])
                              
    Trainingset, Testset = get_trainingsets(hospitals=['Aalborg', 'Aarhus', 'Odense', 'Vejle'])
    class_weights = compute_class_weight(class_weight='balanced',
                                         classes=np.unique(Trainingset['binary_score'].to_numpy()),
                                         y=Trainingset['binary_score'].to_numpy())

    train_generator = train_datagen.flow_from_dataframe(dataframe=Trainingset, 
                                                        shuffle=True,
                                                        x_col="new_abs_path", 
                                                        y_col="binary_score", 
                                                        target_size=(600,600), 
                                                        class_mode="categorical",
                                                        batch_size=6, 
                                                        color_mode="rgb") # set as training data
    test_datagen = ImageDataGenerator(rescale=1./255)
    test_generator = test_datagen.flow_from_dataframe(dataframe=Testset, 
                                                      shuffle=True,
                                                      x_col="new_abs_path", 
                                                      y_col="binary_score", 
                                                      target_size=(600,600), 
                                                      class_mode="categorical",
                                                      batch_size=6, 
                                                      color_mode="rgb") # set as training data

    initial_model = EfficientNetB7(input_shape=train_generator[0][0][0].shape,
                                   include_top = False, 
                                   weights = 'imagenet',
                                   classes=2)
    model = initial_model.output
    model = GlobalAveragePooling2D()(model)
    model = Dropout(rate=0.5)(model)
    predictions = Dense(units=2, 
                        activation="softmax")(model)
    model_final = Model(inputs = initial_model.input, 
                        outputs = predictions)

    model_final.compile(loss='categorical_crossentropy', 
                        optimizer=Adam(learning_rate=0.0001, 
                                       beta_1=0.9, 
                                       beta_2=0.95, 
                                       epsilon=1e-7),
                        metrics=['accuracy'])
    reduce = ReduceLROnPlateau(monitor='val_loss', 
                               factor=0.2, 
                               patience=5, 
                               mode='auto', 
                               verbose=1)
    model_final.fit(train_generator,
                    steps_per_epoch=len(train_generator),
                    epochs=20,
                    callbacks=[reduce],
                    verbose=1,
                    workers=30,
                    class_weight=dict(enumerate(class_weights)),
                    validation_data=test_generator)

    ### Confusion Matrix
    
    predictions = model_final.predict(x=test_generator, 
                                      workers=30, 
                                      verbose=1)
    #y_pred=model.predict(x_test)
    #y_pred = np.round(y_pred)
    y_pred = np.argmax(a=predictions, axis=-1)

    y_true=test_generator.classes

    cm = confusion_matrix(y_true, y_pred)
    print(cm)

    print("faew")