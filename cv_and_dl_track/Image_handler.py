# -*- coding: utf-8 -*-
"""
Created on Mon Aug 22 08:15:45 2022
@authors: Morten Sahlertz & Jacob Kjærager
"""
import warnings
warnings.filterwarnings("ignore")

import math
import warnings
import cv2 as cv
import numpy as np
import pandas as pd
from supporting_function import *
from pathlib import Path
import matplotlib.pyplot as plt
from PIL import Image, ImageDraw
from distutils.ccompiler import new_compiler


class Image_handler:
    def __init__(self,
                 path: Path,
                 #datafile_filename: str,
                 color_for_cropping: tuple = (255, 255, 255)): # White is default 

        self.path = path
        self.image = cv.imread(str(path), cv.IMREAD_UNCHANGED)
        self.width = self.image.shape[1]
        self.height = self.image.shape[0]
        self.get_keypoint_coordinates()
        #self.obtain_breast_coordinates(datafile_filename)
        self.color_for_cropping = color_for_cropping


    def plot_image(self, add_breast_contours: bool = False, add_sternal: bool = False) -> None:
        """Plot the image in the Image_handler.

        This function plots the image connected to the Image_handler. This function also has the possibility
        to plot the connected contour points around the breast plus the nipples.

        :param add_breast_contours: Boolean which decides whether or not to plot contour points, defaults to False
        :type add_breast_contours: bool, optional
        :param add_sternal: Boolean which decides whether or not to plot sternal points, defaults to False
        :type add_sternak: bool, optional
        :return: Nothing
        :rtype: None
        """
        plt.imshow(cv.cvtColor(self.image, cv.COLOR_BGR2RGB))
        if add_breast_contours:
            plt.scatter(self.left_breast_x_coordinates,
                        self.left_breast_y_coordinates,
                        s=25, c='blue', marker='o')
            plt.scatter(self.right_breast_x_coordinates,
                        self.right_breast_y_coordinates,
                        s=25, c='red', marker='o')
            plt.scatter(self.left_nipple['x'],
                        self.left_nipple['y'],
                        s=25, c='yellow', marker='o')
            plt.scatter(self.right_nipple['x'],
                        self.right_nipple['y'],
                        s=25, c='green', marker='o')
        if add_sternal:
            plt.scatter(self.sternal_lower['x'],
                        self.sternal_lower['y'],
                        s=25, c='purple', marker='o')
            plt.scatter(self.sternal_upper['x'],
                        self.sternal_upper['y'],
                        s=25, c='green', marker='o')
        plt.show()


    def crop_to_square(self) -> None:
        """Crops the image to be square.

        This function crops the image to become a square, meaning that the pixels will be remove
        either in the x or y direction depending on if the image is more wide than high and vice
        versa. The crop is based on the keypoints, to make sure that the keypoints stay in the
        image.
        
        :return: Nothing
        :rtype: None
        """
        # Check if width is larger than height
        if self.width > self.height:
            pixels_to_remove = self.width - self.height
            left_to_right_difference = int(np.rint(self.left_breast_x_coordinates.min(axis=1)[0] - (self.width - self.right_breast_x_coordinates.max(axis=1)[0])))
            # If left side is bigger the difference will be higher than zero
            if left_to_right_difference >= 0:
                # If the left to right difference is bigger than the amount of pixels to remove, that amount of pixels will be removed from the left side
                if left_to_right_difference >= pixels_to_remove:
                    # Change keypoints coordinates
                    self.left_breast_x_coordinates = self.left_breast_x_coordinates - pixels_to_remove
                    self.right_breast_x_coordinates = self.right_breast_x_coordinates - pixels_to_remove
                    self.left_nipple['x'] = self.left_nipple['x'] - pixels_to_remove
                    self.right_nipple['x'] = self.right_nipple['x'] - pixels_to_remove
                    self.sternal_upper['x'] = self.sternal_upper['x'] - pixels_to_remove
                    self.sternal_lower['x'] = self.sternal_lower['x'] - pixels_to_remove
                    # Remove pixel columns from image
                    self.image = self.image[:,pixels_to_remove:,:]
                    self.width = self.image.shape[1]
                # Else the left to right difference is removed from the left side and half of the rest pixels is removed from each side
                else:
                    # Calculate rest pixels
                    rest_pixels = (pixels_to_remove-left_to_right_difference) / 2
                    # Change keypoints coordinates
                    self.left_breast_x_coordinates = self.left_breast_x_coordinates - int(np.ceil(rest_pixels)+left_to_right_difference)
                    self.right_breast_x_coordinates = self.right_breast_x_coordinates - int(np.ceil(rest_pixels)+left_to_right_difference)
                    self.left_nipple['x'] = self.left_nipple['x'] - int(np.ceil(rest_pixels)+left_to_right_difference)
                    self.right_nipple['x'] = self.right_nipple['x'] - int(np.ceil(rest_pixels)+left_to_right_difference)
                    self.sternal_upper['x'] = self.sternal_upper['x'] - int(np.ceil(rest_pixels)+left_to_right_difference)
                    self.sternal_lower['x'] = self.sternal_lower['x'] - int(np.ceil(rest_pixels)+left_to_right_difference)
                    # Remove pixel columns from image
                    self.image = self.image[:,int(np.floor(rest_pixels)+left_to_right_difference):-int(np.ceil(rest_pixels)), :]
                    self.width = self.image.shape[1]
            else:
                # If the absolut left to right difference is bigger than the amount of pixels to remove, that amount of pixels will be removed from the right side
                if abs(left_to_right_difference) >= pixels_to_remove:
                    # Remove pixel columns from image
                    self.image = self.image[:,:-pixels_to_remove,:]
                    self.width = self.image.shape[1]
                # Else the left to right difference is removed from the right side and half of the rest pixels is removed from each side
                else:
                    # Calculate rest pixels
                    rest_pixels = (pixels_to_remove-abs(left_to_right_difference)) / 2
                    # Change keypoints coordinates
                    self.left_breast_x_coordinates = self.left_breast_x_coordinates - int(np.ceil(rest_pixels))
                    self.right_breast_x_coordinates = self.right_breast_x_coordinates - int(np.ceil(rest_pixels))
                    self.left_nipple['x'] = self.left_nipple['x'] - int(np.ceil(rest_pixels))
                    self.right_nipple['x'] = self.right_nipple['x'] - int(np.ceil(rest_pixels))
                    self.sternal_upper['x'] = self.sternal_upper['x'] - int(np.ceil(rest_pixels))
                    self.sternal_lower['x'] = self.sternal_lower['x'] - int(np.ceil(rest_pixels))
                    # Remove pixel columns from image
                    self.image = self.image[:,int(np.ceil(rest_pixels)):-int(np.floor(rest_pixels)+abs(left_to_right_difference)), :]
                    self.width = self.image.shape[1]
        # Check if height is larger than width
        elif self.width < self.height:
            pixels_to_remove = self.height - self.width
            top_point = np.min([self.sternal_upper['y'], self.left_breast_y_coordinates.min(axis=1)[0], self.right_breast_y_coordinates.min(axis=1)[0]])
            bottom_point = np.max([self.sternal_lower['y'], self.left_breast_y_coordinates.max(axis=1)[0], self.right_breast_y_coordinates.max(axis=1)[0]])
            top_to_down_difference = int(np.rint(top_point - (self.height - bottom_point)))
            # If top side is bigger the difference will be higher than zero
            if top_to_down_difference >= 0:
                # If the top to down difference is bigger than the amount of pixels to remove, that amount of pixels will be removed from the top
                if top_to_down_difference >= pixels_to_remove:
                    # Change keypoints coordinates
                    self.left_breast_y_coordinates = self.left_breast_y_coordinates - pixels_to_remove
                    self.right_breast_y_coordinates = self.right_breast_y_coordinates - pixels_to_remove
                    self.left_nipple['y'] = self.left_nipple['y'] - pixels_to_remove
                    self.right_nipple['y'] = self.right_nipple['y'] - pixels_to_remove
                    self.sternal_upper['y'] = self.sternal_upper['y'] - pixels_to_remove
                    self.sternal_lower['y'] = self.sternal_lower['y'] - pixels_to_remove
                    # Remove pixel rows from image
                    self.image = self.image[pixels_to_remove:,:,:]
                    self.height = self.image.shape[0]
                else:
                    # Calculate rest pixels
                    rest_pixels = (pixels_to_remove-top_to_down_difference) / 2
                    # Change keypoints coordinates
                    self.left_breast_y_coordinates = self.left_breast_y_coordinates - int(np.ceil(rest_pixels)+top_to_down_difference)
                    self.right_breast_y_coordinates = self.right_breast_y_coordinates - int(np.ceil(rest_pixels)+top_to_down_difference)
                    self.left_nipple['y'] = self.left_nipple['y'] - int(np.ceil(rest_pixels)+top_to_down_difference)
                    self.right_nipple['y'] = self.right_nipple['y'] - int(np.ceil(rest_pixels)+top_to_down_difference)
                    self.sternal_upper['y'] = self.sternal_upper['y'] - int(np.ceil(rest_pixels)+top_to_down_difference)
                    self.sternal_lower['y'] = self.sternal_lower['y'] - int(np.ceil(rest_pixels)+top_to_down_difference)
                    # Remove pixel rows from image
                    self.image = self.image[int(np.floor(rest_pixels)+top_to_down_difference):-int(np.ceil(rest_pixels)),:, :]
                    self.height = self.image.shape[0]
            # If the absolut top to down difference is bigger than the amount of pixels to remove, that amount of pixels will be removed from the bottom
            else:
                if abs(top_to_down_difference) >= pixels_to_remove:
                    # Remove pixel rows from image
                    self.image = self.image[:-pixels_to_remove,:,:]
                    self.height = self.image.shape[0]
                else:
                    # Calculate rest pixels
                    rest_pixels = (pixels_to_remove-abs(top_to_down_difference)) / 2
                    # Change keypoints coordinates
                    self.left_breast_y_coordinates = self.left_breast_y_coordinates - int(np.ceil(rest_pixels))
                    self.right_breast_y_coordinates = self.right_breast_y_coordinates - int(np.ceil(rest_pixels))
                    self.left_nipple['y'] = self.left_nipple['y'] - int(np.ceil(rest_pixels))
                    self.right_nipple['y'] = self.right_nipple['y'] - int(np.ceil(rest_pixels))
                    self.sternal_upper['y'] = self.sternal_upper['y'] - int(np.ceil(rest_pixels))
                    self.sternal_lower['y'] = self.sternal_lower['y'] - int(np.ceil(rest_pixels))
                    # Remove pixel rows from image
                    self.image = self.image[int(np.ceil(rest_pixels)):-int(np.floor(rest_pixels)+abs(top_to_down_difference)),:, :]
                    self.height = self.image.shape[0]

    def crop_image(self) -> bool:
        """Crops the image in the Image_handler.

        This function crops the image connected to the Image_handler. This function is based on the remove_background
        function that removes the background from the image and the crop_image function, then crops the image, so
        that all pixel rows in the x-axis not containing the person is cropped. The keypoints are then updated to fit
        with the cropped image. In case of no foreground found or the cropping cutting out keypoints, no new image
        is generated and the original image is kept. An error message is displayed if the cropping fails.

        :return: Boolean, which is True if cropping was successful otherwise false
        :rtype: bool
        """
        np_rgb_image = remove_background(self.color_for_cropping, self.image)
        summed_rgb_df = pd.DataFrame(np_rgb_image.sum(axis=2))
        cropped_df = summed_rgb_df.loc[summed_rgb_df[summed_rgb_df.sum(axis=1) != 0].index, summed_rgb_df.T[summed_rgb_df.T.sum(axis=1) != 0].index]
        x_cropped = np_rgb_image[cropped_df.index.values, :, :]
        fully_cropped = x_cropped[:, cropped_df.columns.values, :]

        # Generate variables
        try:
            left_crop_column = cropped_df.columns[0]
            high_crop_row = cropped_df.index[0]
            if (self.left_breast_x_coordinates.min(axis=1)[0] - left_crop_column) < 0:
                print("Error cropping image: Left breast cut out of image!")
                print("Keeping original image.")
                print("Image path for error: {}".format(self.path))
                return False
            elif (self.right_breast_x_coordinates.max(axis=1)[0] - left_crop_column) > (cropped_df.columns[-1] - left_crop_column):
                print("Error cropping image: Right breast cut out of image!")
                print("Keeping original image.")
                print("Image path for error: {}".format(self.path))
                print(fully_cropped.shape)
                print(self.right_breast_x_coordinates.max(axis=1)[0])
                return False
            elif (np.min([self.sternal_upper['y'], self.left_breast_y_coordinates.min(axis=1)[0], self.right_breast_y_coordinates.min(axis=1)[0]] - high_crop_row)) < 0:
                print("Error cropping image: Top keypoint cut out of image!")
                print("Keeping original image.")
                print("Image path for error: {}".format(self.path))
                return False
            elif (np.max([self.sternal_lower['y'], self.left_breast_y_coordinates.max(axis=1)[0], self.right_breast_y_coordinates.max(axis=1)[0]] - high_crop_row) > (cropped_df.index[-1] - high_crop_row)):
                print("Error cropping image: Bottom keypoint cut out of image!")
                print("Keeping original image.")
                print("Image path for error: {}".format(self.path))
                print(fully_cropped.shape)
                print(np.max([self.sternal_lower['y'], self.left_breast_y_coordinates.max(axis=1)[0], self.right_breast_y_coordinates.max(axis=1)[0]]))
                return False
            else:
                self.left_breast_x_coordinates = self.left_breast_x_coordinates - left_crop_column
                self.left_breast_y_coordinates = self.left_breast_y_coordinates - high_crop_row

                self.right_breast_x_coordinates = self.right_breast_x_coordinates - left_crop_column
                self.right_breast_y_coordinates = self.right_breast_y_coordinates - high_crop_row

                self.left_nipple["x"] = self.left_nipple["x"] - left_crop_column
                self.left_nipple["y"] = self.left_nipple["y"] - high_crop_row

                self.right_nipple["x"] = self.right_nipple["x"] - left_crop_column
                self.right_nipple["y"] = self.right_nipple["y"] - high_crop_row

                self.sternal_upper['x'] = self.sternal_upper['x'] - left_crop_column
                self.sternal_upper['y'] = self.sternal_upper['y'] - high_crop_row

                self.sternal_lower['x'] = self.sternal_lower['x'] - left_crop_column
                self.sternal_lower['y'] = self.sternal_lower['y'] - high_crop_row    

                self.width = cropped_df.columns[-1] - left_crop_column
                self.height = cropped_df.index[-1] - high_crop_row
                if not amount_of_zeros_above_quantile(fully_cropped, self.path, quantile=0.99):
                    self.image = fully_cropped
                return True
                
        except:
            print("Error cropping image: No foreground found!")
            print("Keeping original image.")
            print("Image path for error: {}".format(self.path))
            return False


    def resize_image(self, pixel_width: int, pixel_height: int) -> None:
        """Resizes the image in the Image_handler.

        This function resizes the image connected to the Image_handler. The function is based on the
        OpenCV resize function. The keypoints are updated to fit with the resized image.

        :param pixel_width: Integer with the new pixel width
        :type pixel_width: int
        :param pixel_height: Integer with the new pixel height
        :type pixel_height: int
        :return: Nothing
        :rtype: None
        """
        width_percentage = pixel_width / self.width
        height_percentage = pixel_height / self.height

        self.left_breast_x_coordinates = self.left_breast_x_coordinates * width_percentage
        self.right_breast_x_coordinates = self.right_breast_x_coordinates * width_percentage
        self.left_breast_y_coordinates = self.left_breast_y_coordinates * height_percentage
        self.right_breast_y_coordinates = self.right_breast_y_coordinates * height_percentage

        self.left_nipple['x'] = self.left_nipple['x'] * width_percentage
        self.right_nipple['x'] = self.right_nipple['x'] * width_percentage
        self.left_nipple['y'] = self.left_nipple['y'] * height_percentage
        self.right_nipple['y'] = self.right_nipple['y'] * height_percentage
        self.width = pixel_width
        self.height = pixel_height

        self.sternal_lower['x'] = self.sternal_lower['x'] * width_percentage
        self.sternal_upper['x'] = self.sternal_upper['x'] * width_percentage
        self.sternal_lower['y'] = self.sternal_lower['y'] * height_percentage
        self.sternal_upper['y'] = self.sternal_upper['y'] * height_percentage

        self.get_pixel_per_cm()

        self.image = cv.resize(self.image, (pixel_width, pixel_height))

    def get_keypoint_coordinates(self) -> None:
        """Get keypoint coordinates from json file.

        This function gets the keypoint coordinates from the json file with the same name as the image,
        except of the file extension of .json instead of .png. Afterwards the keypoints are added to
        the Image_handler. The keypoints consists of nipple points, breast contour and sternal points.

        :return: Nothing
        :rtype: None
        """
        df = pd.read_json(Path(self.path.parent, self.path.as_posix().split('/')[-1].split('.')[0] + '.json'), typ='series', convert_axes=True).to_frame().transpose()

        self.left_nipple, self.left_breast_x_coordinates, self.left_breast_y_coordinates = get_breast_information(side="Left", df=df)
        self.right_nipple, self.right_breast_x_coordinates, self.right_breast_y_coordinates = get_breast_information(side="Right", df=df)

        self.sternal_upper, self.sternal_lower, self.scale_cm = get_sternal_information(df=df)
        self.get_pixel_per_cm()

    def obtain_breast_coordinates(self, datafile_filename: str= "datafile.xml") -> None:
        df = self.read_xml_file()
        correct_paths = []
        for i in df["File Path"]:
            if i.split("/")[-1] == "{}{}".format(str(self.path.stem).lower(), ".jpg"):
                correct_paths.append(i)
        df = df[df["File Path"].isin(correct_paths)]

        #df = df[df['File Path'].str.contains(str(self.path.as_posix()).lower())]
        self.left_nipple, self.left_breast_x_coordinates, self.left_breast_y_coordinates = get_breast_information(side="Left", df=df)
        self.right_nipple, self.right_breast_x_coordinates, self.right_breast_y_coordinates = get_breast_information(side="Right", df=df)

    def generate_keypoint_patches(self, radius_weight: float = 1, shape: str = 'Circle') -> np.array:
        """Generates keypoints patches based on the image in the Image_handler and its keypoints.

        This function generates keypoints patches based on the image in the Image_handler and its corresponding
        keypoints. The radius_weight indicates how big the patch should be, and should be between 0-1. The shape
        decides the shape of the keypoint patch, and should be either a circle or a square. In case of choosing
        a shape name that does not exist it will set to the default value. Afterwards an array contain the
        patches in image form will be returned.

        :param radius_weight: Float to decide the size of the radius, should be between 0-1, defaults to 1
        :type radius_weight: float, optional
        :param shape: String to decide the shape of the keypoints, should be 'Circle' or 'Square', defaults to 'Circle'
        :type shape: str, optional
        :return: NumPy array containing the keypoint patches in image form
        :rtype: np.array
        """
        pixel_radius = int(np.ceil((min(self.image.shape[0:2])/12)*radius_weight))
        keypoints = {}
        
        if shape.lower() != 'square' and shape.lower() != 'circle':
            print('Non possible shape chosen! Please choose between "square" or "circle".')
            print('Shape set to default: Circle')
            shape = 'circle'
        
        
        keypoints["nipple_left_image_patch"] = self.image[int(np.rint(self.left_nipple['y']) - pixel_radius):
                                                          int(np.rint(self.left_nipple['y']) + pixel_radius),
                                                          int(np.rint(self.left_nipple['x']) - pixel_radius):
                                                          int(np.rint(self.left_nipple['x']) + pixel_radius)]
            
        keypoints["nipple_right_image_patch"]= self.image[int(np.rint(self.right_nipple['y']) - pixel_radius):
                                                          int(np.rint(self.right_nipple['y']) + pixel_radius),
                                                          int(np.rint(self.right_nipple['x']) - pixel_radius):
                                                          int(np.rint(self.right_nipple['x']) + pixel_radius)]
        
        keypoints["armpit_left_image_patch"] = self.image[int(np.rint(self.left_breast_y_coordinates['Left contour y1'].iloc[0]) - pixel_radius):
                                                          int(np.rint(self.left_breast_y_coordinates['Left contour y1'].iloc[0]) + pixel_radius),
                                                          int(np.rint(self.left_breast_x_coordinates['Left contour x1'].iloc[0]) - pixel_radius):
                                                          int(np.rint(self.left_breast_x_coordinates['Left contour x1'].iloc[0]) + pixel_radius)]
            
        keypoints["armpit_right_image_patch"] = self.image[int(np.rint(self.right_breast_y_coordinates['Right contour y1'].iloc[0]) - pixel_radius):
                                                           int(np.rint(self.right_breast_y_coordinates['Right contour y1'].iloc[0]) + pixel_radius),
                                                           int(np.rint(self.right_breast_x_coordinates['Right contour x1'].iloc[0]) - pixel_radius):
                                                           int(np.rint(self.right_breast_x_coordinates['Right contour x1'].iloc[0]) + pixel_radius)]    
        
        if shape.lower() == 'circle':
            
            new_keypoints = []
            new_image = Image.new('L', (pixel_radius*2, pixel_radius*2), 0)
            draw = ImageDraw.Draw(new_image)
            draw.pieslice(((0, 0), (pixel_radius*2, pixel_radius*2)), 0, 360, fill = 255)
            np_new_image = np.array(new_image)
            for i in keypoints.keys():
                keypoints[i] = np.dstack((keypoints[i], np_new_image))[:, :, 0:4]
        
        return keypoints

    def get_pixel_per_cm(self) -> None:
        """Gets pixel per cm, from the sternal points.

        This function uses the math.dist function to calculate the distance between the sternal points.
        Then by dividing the scale in centimeters between the points, the pixel per cm is calculated.

        :return: Nothing
        :rtype: None
        """
        self.pixel_per_cm = math.dist([self.sternal_upper['x'], self.sternal_upper['y']],
                                      [self.sternal_lower['x'], self.sternal_lower['y']]) / self.scale_cm     

    def calculate_distance_between_armpits(self) -> float:
        """Calculate the distance between armpits.

        This function uses the math.dist function to calculate the distance between the armpits. Then
        by dividing with the pixel_per_cm calculated from the sternal points, a distance in centimeters
        can be found. This distance will then be returned.

        :return: Float containing the distance between the armpits in centimeters
        :rtype: float
        """     
        return math.dist([self.left_breast_x_coordinates['Left contour x1'].iloc[0],
                          self.left_breast_y_coordinates['Left contour y1'].iloc[0]],
                         [self.right_breast_x_coordinates['Right contour x1'].iloc[0],
                          self.right_breast_y_coordinates['Right contour y1'].iloc[0]]) / self.pixel_per_cm

