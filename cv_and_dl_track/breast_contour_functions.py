

import os
from unittest import skip
import cv2 as cv
import numpy as np
import pandas as pd
import math
import networkx as nx
from tqdm import tqdm
from turtle import left, right
from pathlib import Path
import matplotlib.pyplot as plt
from PIL import Image
from Image_handler import Image_handler
from supporting_function import *
import time

def generate_sobel_image(image: np.array, plot: bool = False) -> np.array:
    """_summary_

    :param image: _description_
    :type image: np.array
    :param plot: _description_, defaults to False
    :type plot: bool, optional
    :return: _description_
    :rtype: np.array
    """
    # Generate grayscale image
    grayscale_image = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
    # Generate sobel for x and y
    sobel_x = cv.Sobel(src=grayscale_image, ddepth=cv.CV_64F, dx=1, dy=0)
    sobel_y = cv.Sobel(src=grayscale_image, ddepth=cv.CV_64F, dx=0, dy=1)
    # Generate sobel for xy (based on the paper from: https://www.researchgate.net/publication/225432142_Breast_Contour_Detection_for_the_Aesthetic_Evaluation_of_Breast_Cancer_Conservative_Treatment)
    sobel_xy = np.sqrt(np.add(np.square(sobel_x), np.square(sobel_y)))
    sobel_xy = cv.convertScaleAbs(sobel_xy)

    if plot:
        plt.imshow(sobel_xy, cmap='gray')
        plt.show()
    # Return the sobel image
    return sobel_xy

def generate_weight_hash_array(alpha: float = 0.15, beta: float = 0.0208, delta: float = 1.85):
    """_summary_

    :param alpha: _description_, defaults to 0.15
    :type alpha: float, optional
    :param beta: _description_, defaults to 0.0208
    :type beta: float, optional
    :param delta: _description_, defaults to 1.85
    :type delta: float, optional
    :return: _description_
    :rtype: _type_
    """
    hash_array = []
    for i in range(256):
        hash_array.append(alpha*math.exp(beta*(255-i))+delta)

    return np.asarray(hash_array)

def add_edges_from_neighbors(image: np.array, graph: nx.Graph, i: int, j: int, weight_hash_array: np.array) -> None:
    """_summary_

    :param image: _description_
    :type image: np.array
    :param graph: _description_
    :type graph: nx.Graph
    :param i: _description_
    :type i: int
    :param j: _description_
    :type j: int
    :param weight_hash_array: _description_
    :type weight_hash_array: np.array
    """
    graph.add_edges_from([(i+j*image.shape[1], (i+1)+j*image.shape[1])], weight=weight_hash_array[min(image[j, i], image[j, i+1])])
    for l in range(-1, 2):
        graph.add_edges_from([(i+j*image.shape[1], (i+l)+(j+1)*image.shape[1])], weight=weight_hash_array[min(image[j+1, i], image[j+1, i+l])])

def generate_graph(image: np.array, left_armpit: tuple[int,int], right_armpit: tuple[int,int],) -> nx.Graph:
    """_summary_

    :param image: _description_
    :type image: np.array
    :return: _description_
    :rtype: nx.Graph
    """
    top_point = min(left_armpit[1], right_armpit[1]) - 3

    # Generate graph with nodes equal to amount of pixels in the image
    graph = nx.empty_graph(n=((image.shape[0])*(image.shape[1])))
    weight_hash_array = generate_weight_hash_array()
    # Run through the image, but skipping the boundary rows and columns as their neighbors are not of importance
    for i in range(1,image.shape[1]-1):
        for j in range(top_point, image.shape[0]-1):
            # Add edges between the neighboring pixels
            add_edges_from_neighbors(image=image, graph=graph, i=i, j=j, weight_hash_array=weight_hash_array)
    # Return the graph
    return graph

def change_node_to_pixel_coordinate(image: np.array, node_nr: int) -> tuple[int, int]:
    """_summary_

    :param image: _description_
    :type image: np.array
    :param node_nr: _description_
    :type node_nr: int
    :return: _description_
    :rtype: int, int
    """
    y = int(np.floor(node_nr/image.shape[1]))
    x = int(node_nr-(y*image.shape[1]))
    return x, y

def change_pixel_coordinate_to_node(image: np.array, point: tuple[int,int]) -> int:
    """_summary_

    :param image: _description_
    :type image: np.array
    :param point: _description_
    :type point: tuple[int,int]
    :return: _description_
    :rtype: int
    """
    return point[0] + point[1]*image.shape[1]

def shortest_path(graph: nx.graph, armpit_node_nr: int, breast_endpoint_node_nr: int) -> list:
    """_summary_

    :param graph: _description_
    :type graph: nx.graph
    :param armpit_node_nr: _description_
    :type armpit_node_nr: int
    :param breast_endpoint_node_nr: _description_
    :type breast_endpoint_node_nr: int
    :return: _description_
    :rtype: list
    """
    return nx.shortest_path(G=graph, source=armpit_node_nr, target=breast_endpoint_node_nr, weight='weight')

def get_xy_coordinates_from_shortest_path(shortest_path: list, image: np.array) -> np.array:
    """_summary_

    :param shortest_path: _description_
    :type shortest_path: _type_
    :param np: _description_
    :type np: _type_
    :return: _description_
    :rtype: _type_
    """
    xy_coordinates = []
    for i in shortest_path:
        xy_coordinates.append(change_node_to_pixel_coordinate(image, i))
    
    x = np.asarray(xy_coordinates)[:,0]
    y = np.asarray(xy_coordinates)[:,1]

    return x, y

def get_midpoint(point_1: tuple[int,int], point_2: tuple[int,int]):
    return [int(np.round((point_1[0]+point_2[0])/2)), int(np.round((point_1[1]+point_2[1])/2))]

def remove_non_essential_nodes(image: np.array, graph: nx.graph, left_armpit: tuple[int,int], right_armpit: tuple[int,int], left_nipple: tuple[int,int], right_nipple: tuple[int,int]) -> None:

    top_point = min(left_armpit[1], right_armpit[1]) - 4

    left_mid_three_fourth = get_midpoint((get_midpoint(left_armpit, get_midpoint(left_armpit, right_armpit))), get_midpoint(left_armpit, right_armpit))
    right_mid_three_fourth = get_midpoint((get_midpoint(right_armpit, get_midpoint(right_armpit, left_armpit))), get_midpoint(right_armpit, left_armpit))

    size_of_linspace_left = max(abs(left_mid_three_fourth[0]-left_nipple[0]), abs(left_mid_three_fourth[1]-left_nipple[1]))*2
    size_of_linspace_right = max(abs(right_mid_three_fourth[0]-right_nipple[0]), abs(right_mid_three_fourth[1]-right_nipple[1]))*2

    x_line_left_nipple = np.linspace(left_mid_three_fourth[0], left_nipple[0], num=size_of_linspace_left)
    y_line_left_nipple = np.linspace(left_mid_three_fourth[1], left_nipple[1], num=size_of_linspace_left)
    x_line_right_nipple = np.linspace(right_mid_three_fourth[0], right_nipple[0], num=size_of_linspace_right)
    y_line_right_nipple = np.linspace(right_mid_three_fourth[1], right_nipple[1], num=size_of_linspace_right)
    
    left_skip_coordinates = pd.DataFrame(np.transpose(np.asarray([x_line_left_nipple.astype(int), y_line_left_nipple.astype(int)])))[[0,1]].value_counts().reset_index(name='c')[[0,1]].to_numpy()
    left_skip_coordinates = np.concatenate((left_skip_coordinates, np.transpose(np.asarray([left_skip_coordinates[:,0] - 1, left_skip_coordinates[:,1]]))))
    left_skip_coordinates = np.concatenate((left_skip_coordinates, np.transpose(np.asarray([left_skip_coordinates[:,0] + 1, left_skip_coordinates[:,1]]))))

    right_skip_coordinates = pd.DataFrame(np.transpose(np.asarray([x_line_right_nipple.astype(int), y_line_right_nipple.astype(int)])))[[0,1]].value_counts().reset_index(name='c')[[0,1]].to_numpy()
    right_skip_coordinates = np.concatenate((right_skip_coordinates, np.transpose(np.asarray([right_skip_coordinates[:,0] - 1, right_skip_coordinates[:,1]]))))
    right_skip_coordinates = np.concatenate((right_skip_coordinates, np.transpose(np.asarray([right_skip_coordinates[:,0] + 1, right_skip_coordinates[:,1]]))))

    for i in range(left_skip_coordinates[-1][1]-top_point):
        np.append(left_skip_coordinates, [[left_skip_coordinates[-1][0], left_skip_coordinates[-1][1] - i]], axis=0)

    for i in range(right_skip_coordinates[-1][1]-top_point):
        np.append(right_skip_coordinates, [[right_skip_coordinates[-1][0], right_skip_coordinates[-1][1] - i]], axis=0)

    skip_nodes = []

    for i in left_skip_coordinates:
        skip_nodes.append(change_pixel_coordinate_to_node(image, i))

    for i in right_skip_coordinates:
        skip_nodes.append(change_pixel_coordinate_to_node(image, i))

    for i in np.unique(skip_nodes):
        graph.remove_node(i)    


def breast_contours_from_keypoints(image: np.array, left_armpit: tuple[int,int], right_armpit: tuple[int,int], left_nipple: tuple[int,int], right_nipple: tuple[int,int], plot: bool = True) -> None:
    sobel_xy = generate_sobel_image(image)

    G = generate_graph(sobel_xy, left_armpit=left_armpit, right_armpit=right_armpit)

    breast_end_point = get_midpoint(left_armpit, right_armpit)

    remove_non_essential_nodes(image=image, graph=G, left_armpit=left_armpit, right_armpit=right_armpit, left_nipple=left_nipple, right_nipple=right_nipple)

    shortest_path_left_breast = shortest_path(G, change_pixel_coordinate_to_node(sobel_xy, left_armpit), change_pixel_coordinate_to_node(sobel_xy, breast_end_point))
    shortest_path_right_breast = shortest_path(G, change_pixel_coordinate_to_node(sobel_xy, right_armpit), change_pixel_coordinate_to_node(sobel_xy, breast_end_point))

    x_left, y_left = get_xy_coordinates_from_shortest_path(shortest_path_left_breast, sobel_xy)
    x_right, y_right = get_xy_coordinates_from_shortest_path(shortest_path_right_breast, sobel_xy)

    if plot:
        plt.imshow(cv.cvtColor(image, cv.COLOR_BGR2RGB))
        plt.plot(x_left, y_left)
        plt.plot(x_right, y_right)
        plt.show()
    

def get_17_contour_points_from_xy_coordinates(x: np.array, y: np.array, side: str) -> pd.DataFrame:
    distance_between_contours = len(x) / 16
    df_x = pd.DataFrame()
    df_y = pd.DataFrame()
    for i in range(16):
        df_x[side + ' contour x' + str(i+1)] = [x[int(np.round(distance_between_contours*i))]]
        df_y[side + ' contour y' + str(i+1)] = [y[int(np.round(distance_between_contours*i))]]

    df_x[side + ' contour x17'] = [x[-1]]
    df_y[side + ' contour y17'] = [y[-1]]
    
    return df_x, df_y
    
def add_edges_from_neighbors_2(image: np.array, graph: nx.Graph, i: int, j: int, weight_hash_array: np.array, skip_coordinates: np.array) -> None:
    """_summary_

    :param image: _description_
    :type image: np.array
    :param graph: _description_
    :type graph: nx.Graph
    :param i: _description_
    :type i: int
    :param j: _description_
    :type j: int
    :param weight_hash_array: _description_
    :type weight_hash_array: np.array
    """
    graph.add_edges_from([(i+j*image.shape[1], (i+1)+j*image.shape[1])], weight=weight_hash_array[min(image[j, i], image[j, i+1])])
    for l in range(-1, 2):
        graph.add_edges_from([(i+j*image.shape[1], (i+l)+(j+1)*image.shape[1])], weight=weight_hash_array[min(image[j+1, i], image[j+1, i+l])])

def generate_graph_2(image: np.array, left_armpit: tuple[int,int], right_armpit: tuple[int,int], left_nipple: tuple[int,int]) -> nx.Graph:
    """_summary_

    :param image: _description_
    :type image: np.array
    :return: _description_
    :rtype: nx.Graph
    """
    mid_three_fourth = get_midpoint((get_midpoint(left_armpit, get_midpoint(left_armpit, right_armpit))), get_midpoint(left_armpit, right_armpit))
    mid_one_fourth = get_midpoint(left_armpit, (get_midpoint(left_armpit, get_midpoint(left_armpit, right_armpit))))

    nipple_three_fourth = (get_midpoint(left_nipple, get_midpoint(left_nipple, mid_three_fourth)))

    size_of_linspace_one = max(abs(mid_three_fourth[0]-mid_one_fourth[0]), abs(mid_three_fourth[1]-mid_one_fourth[1]))*2
    size_of_linspace_two = max(abs(mid_three_fourth[0]-nipple_three_fourth[0]), abs(mid_three_fourth[1]-nipple_three_fourth[1]))*2

    x_line_left_breast = np.linspace(mid_one_fourth[0], mid_three_fourth[0], num=size_of_linspace_one)
    y_line_left_breast = np.linspace(mid_one_fourth[1], mid_three_fourth[1], num=size_of_linspace_one)

    x_line_left_nipple = np.linspace(mid_three_fourth[0], nipple_three_fourth[0], num=size_of_linspace_two)
    y_line_left_nipple = np.linspace(mid_three_fourth[1], nipple_three_fourth[1], num=size_of_linspace_two)

    top_point = min(left_armpit[1], right_armpit[1])

    print(top_point)

    #skip_coordinates_one = pd.DataFrame(np.transpose(np.asarray([x_line_left_breast.astype(int), y_line_left_breast.astype(int)])))[[0,1]].value_counts().reset_index(name='c')[[0,1]].to_numpy()
    skip_coordinates = pd.DataFrame(np.transpose(np.asarray([x_line_left_nipple.astype(int), y_line_left_nipple.astype(int)])))[[0,1]].value_counts().reset_index(name='c')[[0,1]].to_numpy()
    #skip_coordinates = np.concatenate((skip_coordinates_one, skip_coordinates_two))
    skip_coordinates = np.concatenate((skip_coordinates, np.transpose(np.asarray([skip_coordinates[:,0] - 1, skip_coordinates[:,1]]))))
    skip_coordinates = np.concatenate((skip_coordinates, np.transpose(np.asarray([skip_coordinates[:,0] + 1, skip_coordinates[:,1]]))))

    
    return skip_coordinates

    # Generate graph with nodes equal to amount of pixels in the image
    graph = nx.empty_graph(n=((image.shape[0])*(image.shape[1])))
    weight_hash_array = generate_weight_hash_array()
    # Run through the image, but skipping the boundary rows and columns as their neighbors are not of importance
    for i in range(1,image.shape[1]-1):
        for j in range(top_point-3, image.shape[0]-1):
            # Add edges between the neighboring pixels
            add_edges_from_neighbors(image=image, graph=graph, i=i, j=j, weight_hash_array=weight_hash_array)
    # Return the graph
    return graph

def breast_contours_from_keypoints_2(image: np.array, left_armpit: tuple[int,int], right_armpit: tuple[int,int], left_nipple: tuple[int,int], plot: bool = True) -> None:
    sobel_xy = generate_sobel_image(image)

    G = generate_graph_2(sobel_xy, left_armpit, right_armpit, left_nipple)

    breast_end_point = get_midpoint(left_armpit, right_armpit)

    shortest_path_left_breast = shortest_path(G, change_pixel_coordinate_to_node(sobel_xy, left_armpit), change_pixel_coordinate_to_node(sobel_xy, breast_end_point))
    shortest_path_right_breast = shortest_path(G, change_pixel_coordinate_to_node(sobel_xy, right_armpit), change_pixel_coordinate_to_node(sobel_xy, breast_end_point))

    x_left, y_left = get_xy_coordinates_from_shortest_path(shortest_path_left_breast, sobel_xy)
    x_right, y_right = get_xy_coordinates_from_shortest_path(shortest_path_right_breast, sobel_xy)



    if plot:
        plt.imshow(cv.cvtColor(image, cv.COLOR_BGR2RGB))
        plt.plot(x_left, y_left)
        plt.plot(x_right, y_right)
        plt.show()


if __name__ == "__main__":

    #images_data_path = Path("E:/", "Jasper", "BCCTCore2.0", "Aarhus", "650005 year 0.png")
    images_data_path = Path("E:/", "Jasper", "BCCTCore2.0", "Odense", "420001 year 0.png")
    #images_data_path = Path("E:/", "Jasper", "BCCTCore2.0", "Odense", "422018 year 5.png")
    #images_data_path = Path("E:/", "Jasper", "BCCTCore2.0", "Odense", "422008 year 0.png")
    test_image = Image_handler(path = images_data_path)

    percentage = 740 / test_image.width
    test_image.resize_image(int(test_image.width * percentage), int(test_image.height * percentage))

    armpit_left = (int(np.round(test_image.left_breast_x_coordinates['Left contour x1'].iloc[0])), int(np.round(test_image.left_breast_y_coordinates['Left contour y1'].iloc[0])))
    armpit_right = (int(np.round(test_image.right_breast_x_coordinates['Right contour x1'].iloc[0])), int(np.round(test_image.right_breast_y_coordinates['Right contour y1'].iloc[0])))
    nipple_left = (int(np.round(test_image.left_nipple['x'])), int(np.round(test_image.left_nipple['y'])))
    nipple_right = (int(np.round(test_image.right_nipple['x'])), int(np.round(test_image.right_nipple['y'])))

    mid_three_fourth = get_midpoint((get_midpoint(armpit_left, get_midpoint(armpit_left, armpit_right))), get_midpoint(armpit_left, armpit_right))
    mid_one_fourth = get_midpoint(armpit_left, (get_midpoint(armpit_left, get_midpoint(armpit_left, armpit_right))))

    right_mid_three_fourth = get_midpoint((get_midpoint(armpit_right, get_midpoint(armpit_right, armpit_left))), get_midpoint(armpit_right, armpit_left))
    nipple_three_fourth = (get_midpoint(nipple_left, get_midpoint(nipple_left, mid_three_fourth)))

    size_of_linspace = max(abs(mid_three_fourth[0]-mid_one_fourth[0]), abs(mid_three_fourth[1]-mid_one_fourth[1]))*2
    
    x = np.linspace(armpit_right[0], right_mid_three_fourth[0], num=size_of_linspace)
    y = np.linspace(armpit_right[1], right_mid_three_fourth[1], num=size_of_linspace)
    #x = np.linspace(mid_one_fourth[0], mid_three_fourth[0], num=size_of_linspace)
    #y = np.linspace(mid_one_fourth[1], mid_three_fourth[1], num=size_of_linspace)
    x2 = np.linspace(mid_three_fourth[0], nipple_three_fourth[0])
    y2 = np.linspace(mid_three_fourth[1], nipple_three_fourth[1])
    plt.plot(x, y)
    plt.plot(x2, y2)
    plt.imshow(test_image.image)
    plt.show()

    

    sobel_xy = generate_sobel_image(test_image.image)

    testing = generate_graph_2(image=sobel_xy, left_armpit=armpit_left, right_armpit=armpit_right, left_nipple=nipple_left)

    testing = np.append(testing, [[364, 138]], axis=0)
    testing = np.append(testing, [[364, 137]], axis=0)
    testing = np.append(testing, [[364, 136]], axis=0)
    testing = np.append(testing, [[364, 135]], axis=0)
    testing = np.append(testing, [[364, 134]], axis=0)

    skip_nodes = []
    for i in testing:
        skip_nodes.append(change_pixel_coordinate_to_node(sobel_xy, i))

    

    start = time.process_time()
    G = generate_graph(sobel_xy, left_armpit=armpit_left, right_armpit=armpit_right)
    print(time.process_time() - start)

    for i in np.unique(skip_nodes):
        try:
            G.remove_node(i)
        except:
            print('node already removed')

    breast_contours_from_keypoints(image=test_image.image, left_armpit=armpit_left, right_armpit=armpit_right, left_nipple=nipple_left, right_nipple=nipple_right)
    #breast_contours_from_keypoints_2(image=test_image.image, left_armpit=armpit_left, right_armpit=armpit_right, left_nipple=nipple_left)

    node_start = np.round(test_image.left_breast_x_coordinates['Left contour x1'].iloc[0])+test_image.image.shape[1]*np.round(test_image.left_breast_y_coordinates['Left contour y1'].iloc[0])
    node_end = np.round(test_image.left_breast_x_coordinates['Left contour x17'].iloc[0])+test_image.image.shape[1]*np.round(test_image.left_breast_y_coordinates['Left contour y17'].iloc[0])

    node_start_2 = change_pixel_coordinate_to_node(sobel_xy, (int(np.round(test_image.left_breast_x_coordinates['Left contour x1'].iloc[0])), int(np.round(test_image.left_breast_y_coordinates['Left contour y1'].iloc[0]))))

    shortest_path = nx.shortest_path(G, source=int(node_start), target=int(node_end), weight='weight')

    x, y = get_xy_coordinates_from_shortest_path(shortest_path, sobel_xy)

    plt.plot(x, y)
    plt.imshow(test_image.image)
    plt.show()

    df_x, df_y = get_17_contour_points_from_xy_coordinates(x, y, 'Left')

    point1 = [np.round(test_image.left_breast_x_coordinates['Left contour x1'].iloc[0]), np.round(test_image.left_breast_y_coordinates['Left contour y1'].iloc[0])]
    point2 = [np.round(test_image.right_breast_x_coordinates['Right contour x1'].iloc[0]), np.round(test_image.right_breast_y_coordinates['Right contour y1'].iloc[0])]

    mid = get_midpoint(point1, point2)

    plt.imshow(cv.cvtColor(test_image.image, cv.COLOR_BGR2RGB))

    plt.scatter(df_x,
                df_y,
                s=25, c='blue', marker='o')
    plt.show()


    print('asdf')