from __future__ import print_function

import os
import cv2 as cv
import numpy as np
import pandas as pd
import math
import networkx as nx
from tqdm import tqdm
from turtle import left
from pathlib import Path
import matplotlib.pyplot as plt
from PIL import Image
from Image_handler import Image_handler
from supporting_function import *

def calculate_weight(pixel_value: float, neighbor_pixel_value: float, alpha: float = 0.15, beta: float = 0.0208, delta: float = 1.85) -> float:
    """_summary_

    :param pixel_value: _description_
    :type pixel_value: float
    :param neighbor_pixel_value: _description_
    :type neighbor_pixel_value: float
    :param alpha: _description_, defaults to 0.15
    :type alpha: float, optional
    :param beta: _description_, defaults to 0.0208
    :type beta: float, optional
    :param delta: _description_, defaults to 1.85
    :type delta: float, optional
    :return: _description_
    :rtype: float
    """
    return(alpha*math.exp(beta*(255-min(pixel_value, neighbor_pixel_value)))+delta)

def add_edges_from_neighbors(image: np.array, graph: nx.Graph, i: int, j: int) -> None:
    """_summary_

    :param image: _description_
    :type image: np.array
    :param graph: _description_
    :type graph: nx.Graph
    :param i: _description_
    :type i: int
    :param j: _description_
    :type j: int
    """
    # Run through the 8 neighboring pixels
    for l in range(-1, 2):
        for g in range(-1, 2):
            # Skip if it is the center pixel
            if l == 0 and g == 0:
                continue
            # Add edges between neighboring pixels, with the calculated weight
            else:
                graph.add_edges_from([(i+j*image.shape[1], i+l+(j+g)*image.shape[1])],
                                     weight=calculate_weight(image[j, i], image[j+g, i+l]))

def generate_graph(image: np.array) -> nx.Graph:
    """_summary_

    :param image: _description_
    :type image: np.array
    :return: _description_
    :rtype: nx.Graph
    """
    # Generate graph with nodes equal to amount of pixels in the image
    graph = nx.empty_graph(n=((image.shape[0])*(image.shape[1])))
    # Run through the image, but skipping the boundary rows and columns as their neighbors are not of importance
    for i in range(1,image.shape[1]-1):
        for j in range(1,image.shape[0]-1):
            # Add edges between the neighboring pixels
            add_edges_from_neighbors(image=image, graph=graph, i=i, j=j)
    # Return the graph
    return graph

def get_midpoint(point_1: np.array, point_2: np.array):
    return [(point_1[0]+point_2[0])/2, (point_1[1]+point_2[1])/2]

if __name__ == "__main__":

    df = pd.read_csv(Path("E:/", "Jasper", "BCCTCore2.0", "Image analysis, data on endpoints, year 0-5.csv"), sep=';')
    df = df.rename(columns={'rand_no': "Randomization number"})
    df = df.drop_duplicates(subset=['Randomization number'], keep='first')

    read = pd.read_pickle(Path("E:/", "Jasper", "BCCTCore2.0", "temporary_folder", "merged_bcct_core_and_admin_files.pkl"))
    merged = pd.merge(read, df, on="Randomization number", how='inner')

    new_df = merged[merged['Year'] != 6].reset_index(drop=True)

    new_df = new_df[~((new_df['Year'] == 0) & (new_df['cosmetic_bin_0'].isnull()))].reset_index(drop=True)
    new_df = new_df[~((new_df['Year'] == 1) & (new_df['cosmetic_bin_1'].isnull()))].reset_index(drop=True)
    new_df = new_df[~((new_df['Year'] == 2) & (new_df['cosmetic_bin_2'].isnull()))].reset_index(drop=True)
    new_df = new_df[~((new_df['Year'] == 3) & (new_df['cosmetic_bin_3'].isnull()))].reset_index(drop=True)
    new_df = new_df[~((new_df['Year'] == 4) & (new_df['cosmetic_bin_4'].isnull()))].reset_index(drop=True)
    new_df = new_df[~((new_df['Year'] == 5) & (new_df['cosmetic_bin_5'].isnull()))].reset_index(drop=True)

    new_df['binary_score'] = 0

    for index in new_df.index:
        row = new_df.loc[index]
        if row['cosmetic_bin_' + str(row['Year'])] == 'Grade 2/3':
            new_df.at[index, 'binary_score'] = 1

    new_df = new_df[~(new_df['Institution'] == 'DresdenCGC')].reset_index(drop=True)
    
    # Can't find this image
    dropped_index = 1637
    new_df = new_df.drop(new_df.index[dropped_index])


    new_file_path = Path("E:/", "Jasper", "BCCTCore2.0", "testing_new2")
    images_data_path = Path("E:/", "Jasper", "BCCTCore2.0")

    for index in tqdm(new_df.index):
        row = new_df.loc[index]
        curr_image = Image_handler(Path(images_data_path, row['Institution'], str(row["Randomization number"]) + " year " + str(row["Year"]) + '.png'))
        curr_image.crop_to_square()
        curr_image.resize_image(600, 600)
        temp_img = Image.fromarray(cv.cvtColor(curr_image.image, cv.COLOR_BGR2RGB), mode='RGB')
        directory = Path(new_file_path)
        os.chdir(directory)
        name = str(row["Randomization number"]) + " year " + str(row["Year"])
        temp_img.save(name + ".png")

    print('Done')


 