# -*- coding: utf-8 -*-
"""
Created on Mon Aug 22 08:15:45 2022
@authors: Morten Sahlertz & Jacob Kjærager
"""

import warnings
warnings.filterwarnings("ignore")

import os
import cv2 as cv
import numpy as np
import pandas as pd
from tqdm import tqdm
from turtle import left
from pathlib import Path
import matplotlib.pyplot as plt
from PIL import Image
from plotly.offline import plot
import plotly.graph_objects as go
from dataimport import import_data
from Image_handler import Image_handler
from typing import Dict, List, Tuple, Union

# %%

if __name__ == "__main__":
    pth = "E:/Jasper/BCCTCore2.0/Aarhus"
    for img_name in tqdm(os.listdir(pth)):
        if ".json" not in img_name:
            img  = Image_handler(Path("{}/{}".format(pth, img_name)))
            img.crop_image()
            print("faew")
    cropping_problem = [Path("E:/", "Jasper", "BCCTCore2.0", "DresdenCGC", "500026 year 3.png"),
                        Path("E:/", "Jasper", "BCCTCore2.0", "DresdenCGC", "501001 year 0.png"),
                        Path("E:/", "Jasper", "BCCTCore2.0", "DresdenCGC", "501030 year 4.png"),
                        Path("E:/", "Jasper", "BCCTCore2.0", "DresdenCGC", "503007 year 4.png")]
    white = (255, 255, 255)
    test = Image_handler(Path(cropping_problem[0]))
    test.crop_image()
    test.check_image_for_too_much_background()
    image_values = {}
    for img_name in tqdm(os.listdir(pth)):
        img  = Image.open(("{}/{}").format(pth, img_name))
        img.load()
        data = np.asarray(img, dtype="int32")
        image_values[img_name] = np.prod(data.shape)-np.count_nonzero(data)
    df = pd.DataFrame.from_dict(image_values, orient='index', columns=["image_decimal_value"])
    outliers = df[~(df["image_decimal_value"] > df["image_decimal_value"].quantile(0.01)) 
                  #& (df["image_decimal_value"] < df["image_decimal_value"].quantile(0.99))
                 ]
    print(outliers.shape)
    for outlier_imgs in tqdm(outliers.index):
        img  = Image.open(("{}/{}").format(pth, outlier_imgs))
        img.show()
    merged = import_data()
    cropping_problem = [Path("E:/", "Jasper", "BCCTCore2.0", "DresdenCGC", "500026 year 3.png"),
                        Path("E:/", "Jasper", "BCCTCore2.0", "DresdenCGC", "501001 year 0.png"),
                        Path("E:/", "Jasper", "BCCTCore2.0", "DresdenCGC", "501030 year 4.png"),
                        Path("E:/", "Jasper", "BCCTCore2.0", "DresdenCGC", "503007 year 4.png")]
    white = (255, 255, 255)
    test = Image_handler(Path(cropping_problem[0]))
    test.crop_image()

    img_path = Path("E:/", "Jasper", "BCCTCore2.0", "Aarhus", "650001 year 0.png")
    img_path = Path("E:/", "Jasper", "BCCTCore2.0", "Aalborg", "809020 year 4.png")
    img_path = Path('E:\\Jasper\\BCCTCore2.0\\Aarhus\\702013 year 1.png')
    img_path2 = Path('E:\\Jasper\\BCCTCore2.0\\Odense\\425012 year 0.png')
    
    test2 = Image_handler(Path(img_path))
    test3 = Image_handler(Path(img_path2))

    images = []
    all_cities = []
    all_height = []
    all_width = []
    all_ratio = []
    all_armpit_length = []
    hospitals = ["DresdenCGC", "Aalborg"]
    images_data_path = Path("E:/", "Jasper", "BCCTCore2.0")

    new_file_path = Path("E:/", "Jasper", "BCCTCore2.0", "Testing")
    #fig, ax = plt.subplots()
    #ax.set_title('Boxplot for armpit length in the corresponding hospitals')
    
    for i in range(len(hospitals)):
        city_armpit_length = []
        for path, subdirs, files in os.walk(Path(images_data_path, hospitals[i])):
            print(path)
            for name in files:
                if name.lower()[-4:] == '.png':
                    curr_image = Image_handler(Path(path, name))
                    curr_image.crop_image()
                    curr_image.crop_to_square()
                    curr_image.resize_image(600, 600)
                    #images.append(curr_image)
                    temp_img = Image.fromarray(cv.cvtColor(curr_image.image, cv.COLOR_BGR2RGB), mode='RGB')
                    directory = Path(new_file_path)
                    os.chdir(directory)
                    temp_img.save(curr_image.path.name)
                    #if (curr_image.width / curr_image.height) < 0.5:
                    #    curr_image.plot_image()
                    #all_armpit_length.append(curr_image.calculate_distance_between_armpits())
                    #city_armpit_length.append(curr_image.calculate_distance_between_armpits())
        #all_cities.append(city_armpit_length)
        
    all_cities.append(all_armpit_length)
    hospitals.append("All Hospitals")
    ax.boxplot(all_cities)
       
    plt.ylabel("Distance between armpits (cm)")
    plt.xticks(range(1, len(hospitals)+1), hospitals)
    plt.xlabel("Hospital:")
    plt.show()
    print("fffw")

    #all_images_path = []
    all_heigth = []
    all_width = []
    all_armpit_length = []
    images_data_path = Path("E:/", "Jasper", "BCCTCore2.0")
    all_images_path = pd.read_csv(Path(images_data_path, 'image_paths.csv'), index_col=0, squeeze=True)

    for i in range(len(all_images_path)):
        curr_image = Image_handler(Path(all_images_path.iloc[i]))
        all_heigth.append(curr_image.height)
        all_width.append(curr_image.width)
        all_armpit_length.append(curr_image.calculate_distance_between_armpits())

    
    for path, subdirs, files in os.walk(images_data_path):
        print(path)
        for name in files:
            if name.lower()[-4:] == '.png':
                #all_images_path.append(Path(path, name))
                curr_image = Image_handler(Path(path, name))
                all_heigth.append(curr_image.height)
                all_width.append(curr_image.width)
                all_armpit_length.append(curr_image.calculate_distance_between_armpits())

                

    img_path = Path("E:/", "Jasper", "BCCTCore2.0", "Aarhus", "650001 year 0.png")
    #hmm = pd.read_json(check, typ='series', dtype=True, convert_axes=True).to_frame().transpose()

    test = Image_handler(img_path)

    new_file_path = "E:\\Jasper\\BCCTCore2.0"
    erro = []
    test = []
    dropped_index = 1663
    for index in tqdm(merged.index[dropped_index+1:]):
        #print(index)
        row = merged.loc[index]
        temp_img = Image.open(row["repeated path"])
        directory = Path(new_file_path, row["Institution"])
        os.chdir(directory)
        name = str(row["Randomization number"]) + " year " + str(row["Year"])
        temp_img.save(name + ".png")
        row.to_json(Path(directory, name + '.json'))
    print("faew")
# %%
