# -*- coding: utf-8 -*-
"""
Created on Mon Aug 22 08:15:45 2022
@authors: Morten Sahlertz & Jacob Kjærager
"""

import warnings
warnings.filterwarnings("ignore")
import os
import cv2 as cv
import numpy as np
import pandas as pd
import SimpleITK as sitk
from pathlib import Path
import matplotlib.pyplot as plt
import xml.etree.ElementTree as ET
from typing import  List, Union
from distutils.ccompiler import new_compiler
from cvzone.SelfiSegmentationModule import SelfiSegmentation


def amount_of_zeros_below_dataset_quantile(image, quantile: int = 99) -> bool:

    #Manully found the next two variables across full dataset
    bottom_ten_quantile= {0: 0, 1: 295.44, 2: 24771.10, 3: 41061.02, 4: 50390.56, 
                            5: 58145.85, 6: 64237.24, 7: 69566.68, 8: 73342.68, 9: 77229.8}
    top_ten_quantile= {90: 217493.70, 91: 221148.11, 92: 225239.52, 93: 230092.39, 94: 233963.88, 
                        95: 238322.75, 96: 244491.00, 97: 252692.64, 98: 265483.44, 99: 288985.13}
    data = np.asarray(image, dtype="int32")
    number_of_zero_pixels_in_image = np.prod(data.shape)-np.count_nonzero(data)
    if number_of_zero_pixels_in_image > top_ten_quantile[quantile]:
        return False
    else:
        return True


def get_breast_information(side: str, df: pd.DataFrame) -> List[Union[dict, pd.DataFrame, pd.DataFrame]]:
    """Get breast information from DataFrame

    This function gets breast information from the DataFrame used in the call. Besides
    the DataFrame a string with a side is needed. This string should be either "Left" or
    "Right". It then extracts the data from the DataFrame and returns the breast information
    in a List containing a Dictionary with the nipple coordinates and two DataFrames with
    the breast contours x and y coordinates.

    :param side: String choosing which side the information should come from, should be "Left" or "Right"
    :type side: str
    :param df: DataFrame containing the DataFrame generated from the image's corresponding json file
    :type df: pd.DataFrame
    :return: List containing a dict with nipple coordinates and two DataFrames with breast contours x and y coordinates
    :rtype: List[Union[dict, pd.DataFrame, pd.DataFrame]]
    """
    right_group = df[df.columns[df.columns.str.contains(side)]]
    df_nipple = df[right_group.columns[right_group.columns.str.contains("nipple")]].astype(float)
    nipple = {"x": df_nipple["{} nipple x".format(side)].iloc[0],
                "y": df_nipple["{} nipple y".format(side)].iloc[0]}
    breast_x_coordinates = df[right_group.columns[right_group.columns.str.contains("x")]].drop(columns=['{} nipple x'.format(side)]).astype(float)
    breast_y_coordinates = df[right_group.columns[right_group.columns.str.contains("y")]].drop(columns=['{} nipple y'.format(side)]).astype(float)
    return [nipple, breast_x_coordinates, breast_y_coordinates]

def get_sternal_information(df: pd.DataFrame) -> List[Union[dict, dict, float]]:
    """Get sternal information from DataFrame

    This function gets sternal information from the DataFrame used in the call. It then
    extracts the data from the DataFrame and returns the sternal information in a List 
    containing two Dictionary with the sternal point coordinates and a Float with
    the extracted scale in centimeters.

    :param df: DataFrame containing the DataFrame generated from the image's corresponding json file
    :type df: pd.DataFrame
    :return: List containing two dicts with sternal point coordinates and a float with the scale in centimeters
    :rtype: List[Union[dict, dict, float]]
    """
    df_sternal = df[df.columns[df.columns.str.contains("Sternal")]].astype(float)
    sternal_upper = {"x": df_sternal["Sternal notch x"].iloc[0],
                        "y": df_sternal["Sternal notch y"].iloc[0]}
    
    df_scale = df[df.columns[df.columns.str.contains("Scale")]].astype(float)
    sternal_lower = {"x": df_scale["Scale x"].iloc[0],
                        "y": df_scale["Scale y"].iloc[0]}
    
    scale_cm = df_scale['Scale (cm)'].iloc[0]
    
    return [sternal_upper, sternal_lower, scale_cm]

def remove_background(color_for_cropping: tuple, image: any, threshold: float = 0.4) -> np.array:

    segmentor = SelfiSegmentation()
    image_no_background = segmentor.removeBG(image,
                                             imgBg=color_for_cropping,
                                             threshold=threshold)
    im_bw = cv.cvtColor(image_no_background,
                        cv.COLOR_BGR2GRAY)  # greyscale the image for single channel images: greylevel = (R+B+G)/3
    grey_level_of_color_for_cropping = sum(color_for_cropping)/len(color_for_cropping)
    im_bw[im_bw != grey_level_of_color_for_cropping] = 1
    im_bw[im_bw == grey_level_of_color_for_cropping] = 0
    holefill_filter = sitk.BinaryFillholeImageFilter()
    holefill_filter.SetForegroundValue(1)
    sitk_np = sitk.GetImageFromArray(np.ascontiguousarray(im_bw))
    filled_mask = holefill_filter.Execute(sitk_np)
    numpy_matrix_of_image = sitk.GetArrayFromImage(filled_mask)

    return (image * numpy_matrix_of_image[..., None])

def remove_test(color_for_cropping, image, threshold):
    segmentor = SelfiSegmentation()
    image_no_background = segmentor.removeBG(image,
                                             imgBg=color_for_cropping,
                                             threshold=threshold)
    return image_no_background

def plot_keypoints(keypoints: np.array) -> None:
    """Plots keypoints one at a time

    This function plots the generated keypoints.

    :param keypoints: NumPy array with images of the generated keypoints
    :type keypoints: np.array
    :return: Nothing
    :rtype: None
    """
    for i in keypoints.keys():
        keypoints[i][:,:,0:3] = cv.cvtColor(keypoints[i][:,:,0:3], cv.COLOR_BGR2RGB)
        plt.imshow(keypoints[i])
        plt.show()
    #keypoints = cv.cvtColor(keypoints, cv.COLOR_BGR2RGB)
    #plt.imshow(keypoints)
    #plt.show()