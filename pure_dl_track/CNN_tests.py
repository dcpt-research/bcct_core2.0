from typing import List, Union
import pandas as pd
import os
import numpy as np
from keras_preprocessing.image import ImageDataGenerator
from tensorflow.keras.applications import EfficientNetB0
from keras.callbacks import ReduceLROnPlateau
from keras.models import Model
from operator import itemgetter 
from tensorflow.keras.optimizers import Adam
from keras.layers import Dense, GlobalAveragePooling2D, Dropout
from sklearn.model_selection import KFold

def construct_set(keys: any, group: any) -> pd.DataFrame:
    to_concat = []
    group_keys = list(group.groups.keys())
    for i in itemgetter(*keys)(group_keys):
        to_concat.append(group.get_group(i))
    return pd.concat(to_concat)

if __name__ == "__main__":
    base_path = "E:\\Jasper\\BCCTCore2.0\\temporary_folder"
    df = pd.read_pickle(base_path+ "\\merged_bcct_core_and_admin_files.pkl")
    df["new_abs_path"] = "E:\\Jasper\\BCCTCore2.0\\Testing\\" + df['Randomization number'].astype(str) + " year " + df['Year'].astype(str)+ ".png"
    train_datagen=ImageDataGenerator(rescale=1./255,
                                     width_shift_range=[-10,10], 
                                     height_shift_range=0.05, 
                                     vertical_flip=True, 
                                     rotation_range=0, 
                                     brightness_range=[0.8,1.2],
                                     zoom_range=[0.8,1.1])
    grouped = df.groupby('Randomization number')
    kf = KFold(n_splits=5, shuffle=True)

    for train_index, test_index in kf.split(list(grouped.groups.keys())):
        Trainingset = construct_set(train_index, grouped)
        Testset = construct_set(test_index, grouped)
        train_generator = train_datagen.flow_from_dataframe(
            dataframe=Trainingset, 
            shuffle=True,
            x_col="new_abs_path", 
            y_col="Overall classification", 
            target_size=(600,600), 
            class_mode="categorical",
            batch_size=30, 
            color_mode="rgb") # set as training data

        initial_model = EfficientNetB0(input_shape=train_generator[0][0][0].shape,
                                       include_top = False, 
                                       weights = 'imagenet',
                                       classes=4)
        model = initial_model.output
        model = GlobalAveragePooling2D()(model)
        model = Dense(1024, activation="relu")(model)
        model = Dropout(0.5)(model)
        predictions = Dense(4, activation="softmax")(model)
        model_final = Model(inputs = initial_model.input, outputs = predictions)

        model_final.compile(loss='categorical_crossentropy', 
                            optimizer=Adam(learning_rate=0.0001, 
                                        beta_1=0.9, 
                                        beta_2=0.95, 
                                        epsilon=1e-7),
                            metrics=['accuracy'])
        reduce = ReduceLROnPlateau(monitor='val_loss', 
                                factor=0.2, 
                                patience=4, 
                                mode='auto', 
                                verbose=1)
        model_final.fit(train_generator,
                        steps_per_epoch=len(train_generator),
                        epochs=1,
                        callbacks=[reduce],
                        verbose=1)
        print("faew")