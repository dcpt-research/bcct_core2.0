import pandas as pd
from keras_preprocessing.image import ImageDataGenerator

def get_generator(set: pd.DataFrame, generator: str = "train") -> ImageDataGenerator:
    if generator == "test":
        test_datagen = ImageDataGenerator()
        generator = test_datagen.flow_from_dataframe(dataframe=set, 
                                                     shuffle=False,
                                                     x_col="new_abs_path", 
                                                     y_col="binary_score", 
                                                     target_size=(600,600), 
                                                     class_mode="categorical",
                                                     batch_size=6, 
                                                     color_mode="rgb")
    else: #defaults to training
        train_datagen=ImageDataGenerator(width_shift_range=[-0.03, 0.03], 
                                         height_shift_range=[-0.03, 0.03], 
                                         horizontal_flip=True, 
                                         brightness_range=[0.75, 1.25],
                                         zoom_range=[0.95, 1.05])
        generator = train_datagen.flow_from_dataframe(dataframe=set, 
                                                      shuffle=True,
                                                      x_col="new_abs_path", 
                                                      y_col="binary_score", 
                                                      target_size=(600,600), 
                                                      class_mode="categorical",
                                                      batch_size=6, 
                                                      color_mode="rgb")

    return generator