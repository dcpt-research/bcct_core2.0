import pandas as pd
from typing import List, Tuple, Union
from pathlib import Path
from sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold

def get_equal_set(training_set: pd.DataFrame) -> pd.DataFrame:

    amount_of_ones = training_set[training_set['binary_score'] == '1'].shape[0]
    training_set_ones = training_set[training_set['binary_score'] == '1']
    training_set_zeros = training_set[training_set['binary_score'] == '0'][0:amount_of_ones]

    return pd.concat([training_set_ones, training_set_zeros]).reset_index(drop=True)


def construct_set(keys: list, group: list) -> pd.DataFrame:
    to_concat = []
    for key in keys:
        to_concat.append(group.get_group(key))
    return pd.concat(to_concat)

def get_train_test_sets(hospitals: list, datafilename: str, K_fold_iterations: int = 5, enable_K_fold: bool = False) -> Tuple[pd.DataFrame, pd.DataFrame]:
    kf = KFold(n_splits=K_fold_iterations)
    this_filepath = Path(__file__).parent.parent #Change this if project routing tree is changed 
    df = pd.read_pickle(Path(this_filepath, datafilename))
    df["new_abs_path"] = Path(this_filepath.parent, "testing_new").as_posix() + "//" + df['Randomization number'].astype(str) + " year " + df['Year'].astype(str)+ ".png"
    df["binary_score"] = df["binary_score"].astype(str)
    all_sets = []
    
    # Generate groups                              
    individual_trainsets = []
    individual_testsets = []
    if enable_K_fold:
        for hospital in hospitals: 
            patients = []
            for patient_group in df[df['Institution'] == hospital].groupby('Randomization number'): #no overlapping patientes between test and trainsets
                patients.append(patient_group[1])
            # Generate train data
            for train_i, test_i in kf.split(patients):
                train_set = pd.concat(pd.Series(patients)[train_i].to_list())
                test_set = pd.concat(pd.Series(patients)[test_i].to_list())
                individual_trainsets.append(train_set)
                individual_testsets.append(test_set)
        for n in range(0, K_fold_iterations):
            all_sets.append((pd.concat(individual_trainsets[n::K_fold_iterations]), 
                             pd.DataFrame(),
                             pd.concat(individual_testsets[n::K_fold_iterations])))
    else:
        individual_cvsets = []
        for hospital in hospitals:  
            patients = []
            
            # Generate groups
            for i, patient in df[df['Institution'] == hospital].groupby('Randomization number'): #no overlapping patientes between test and trainsets
                patients.append(patient)
                                          
            # Generate train data
            train_dfs, intermediate_dfs = train_test_split(patients, 
                                                           test_size=(1/K_fold_iterations), #K_fold_iterations controls the train/test relationship when K_fold is disabled 
                                                           shuffle=True)
            cv_dfs, test_dfs = train_test_split(intermediate_dfs, 
                                                test_size=0.5)            
            individual_trainsets.append(pd.concat(train_dfs))
            individual_cvsets.append(pd.concat(cv_dfs))
            individual_testsets.append(pd.concat(test_dfs))
        all_sets.append((pd.concat(individual_trainsets), 
                         pd.concat(individual_cvsets), 
                         pd.concat(individual_testsets)))
        
    return all_sets

