import numpy as np
import pandas as pd
from pathlib import Path
from sklearn.utils.class_weight import compute_class_weight
from tensorflow.keras.applications import EfficientNetB0, EfficientNetB1, EfficientNetB4, EfficientNetB7, InceptionV3
from tensorflow.keras.callbacks import ReduceLROnPlateau, LearningRateScheduler, ModelCheckpoint
from tensorflow.keras.models import Model
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.layers import Dense, GlobalAveragePooling2D, Dropout
from generators import get_generator

def scheduler(epoch, lr):
    if epoch < 3:
        return lr
    else:
        lr = 0.0000050
        return lr

def construct_and_fit_model(trainset: pd.DataFrame, cv_set: pd.DataFrame, testset: pd.DataFrame) -> Model:
    class_weights = compute_class_weight(class_weight='balanced',
                                         classes=np.unique(trainset['binary_score'].to_numpy()),
                                         y=trainset['binary_score'].to_numpy())
    train_generator = get_generator(set=trainset)

    if cv_set.empty:
        print("Validates model on cross validationset")
        val_data = get_generator(set=cv_set)
    else: 
        print("Validates model on testset")
        val_data = get_generator(set=testset, generator="test")

    initial_model = EfficientNetB7(input_shape=train_generator[0][0][0].shape,
                                    include_top = False, 
                                    weights = 'imagenet',
                                    classes=2)
    model = initial_model.output
    model = GlobalAveragePooling2D()(model)
    model = Dropout(rate=0.5)(model)
    predictions = Dense(units=2, 
                        activation="softmax")(model)
    model_final = Model(inputs = initial_model.input, 
                        outputs = predictions)
    model_final.compile(loss='categorical_crossentropy', 
                        optimizer=Adam(learning_rate=0.0000010, 
                                       beta_1=0.9, 
                                       beta_2=0.999, 
                                       epsilon=1e-7),
                        metrics=['accuracy'])
    reduce = ReduceLROnPlateau(monitor='val_loss', 
                               factor=0.5, 
                               patience=15, 
                               mode='auto', 
                               verbose=1)
                               
    filepath_loss = Path(Path(__file__).parent, "models", "best_loss.hdf5")
    checkpoint_loss = ModelCheckpoint(filepath_loss, monitor='val_loss', verbose=1, save_best_only=True, save_weights_only=False, mode='auto', save_freq="epoch")
    filepath_acc = Path(Path(__file__).parent, "models", "best_acc.hdf5")
    checkpoint_acc = ModelCheckpoint(filepath_acc, monitor='val_accuracy', verbose=1, save_best_only=True, save_weights_only=False, mode='auto', save_freq="epoch")

    callbacks = [checkpoint_loss, checkpoint_acc, reduce]
                               
    #callback = LearningRateScheduler(scheduler)

    model_final.fit(train_generator,
                    steps_per_epoch=len(train_generator),
                    epochs=200,
                    callbacks=[callbacks],
                    verbose=1,
                    workers=32,
                    class_weight=dict(enumerate(class_weights)),
                    validation_data=val_data)

    return model_final
