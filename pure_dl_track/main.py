import os
import numpy as np
import pandas as pd
from pathlib import Path
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from train_test_import import get_train_test_sets, get_equal_set
from model_conf import construct_and_fit_model
from generators import get_generator


if __name__ == "__main__":
    #os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'
    trained_models = []
    os.environ['CUDA_VISIBLE_DEVICES'] = '0'
    sets = get_train_test_sets(hospitals=['Aalborg', 'Aarhus', 'Odense', 'Vejle'], 
                               datafilename="merged_file_for_testing.pkl",
                               K_fold_iterations=5,
                               enable_K_fold=False)
    #equal_set = get_equal_set(training_set=sets[0][0])
    for trainset, cv_set, testset in sets:
        model_final = construct_and_fit_model(trainset=trainset, 
                                              cv_set=cv_set, 
                                              testset=testset)
        trained_models.append(model_final)
    ### Confusion Matrix
    test_generator = get_generator(set=sets[0][2], generator="test")
    predictions = trained_models[0].predict(x=test_generator, 
                                            workers=32, 
                                            verbose=1)
    #y_pred=model.predict(x_test)
    #y_pred = np.round(y_pred)
    
    y_pred = np.argmax(predictions, axis=1)
    y_true=test_generator.classes
    cm = confusion_matrix(y_true, y_pred)
    print(cm)
    all = np.append(y_pred, y_true)
    pd.DataFrame(predictions).to_csv(Path(Path(__file__).parent, 'predictions.csv'))
    pd.DataFrame(all).to_csv(Path(Path(__file__).parent, 'predictions2.csv'))

    print("faew")