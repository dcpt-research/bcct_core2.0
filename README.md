# Master_JK_MS_DCPT
This project is the codebase for the masterproject conducted by Jacob Kjærager and Morten Sahlertz from Aarhus University in the Fall of 2022. The project is written for the Danish Center for Particle Therapy at AUH, with the goal of predicting H2S production in underground seweage systems.
