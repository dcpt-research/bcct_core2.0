import os
import numpy as np
import pandas as pd
from pathlib import Path
from bs4 import BeautifulSoup as bs


def manipulate_string(encoded_string: str, decode_dict: dict) -> str:

    for i in decode_dict.keys():
        encoded_string = encoded_string.replace(i, decode_dict[i])
    return encoded_string

def import_data(hospitals: list = ["Aalborg", "Aarhus", "Dresden CGC", "Odense", "Vejle"], 
                from_directory: str = "E:\\Jasper\\DBCGRT", 
                save_directory: str = "E:\\Jasper\\BCCTCore2.0\\temporary_folder", 
                localxml_db_filename: str = 'BCCT.core.database.bct.xml', 
                make_the_big_run: bool = False)-> pd.DataFrame:
    if make_the_big_run:
        cnt = 0
        percenter= 0
        collected_df = []
        errors = []
        dfs = []
        for hospital in hospitals:
            bool_vector= []
            hospital_data_path = "{}\\{}\\BCCT HYPO {} 03.2018".format(from_directory, hospital, hospital)
            if ((hospital == "Odense") | (hospital == "Aarhus") | (hospital == "Vejle")):
                filepath = "{} 2.xlsx".format(hospital_data_path)
            else: 
                filepath = "{}.xlsx".format(hospital_data_path)
            xl_file = pd.read_excel(filepath)
            df_with_path = xl_file[~xl_file["repeated path"].isna()]
            for i in df_with_path.index:
                abs_path = "{}\\{}\\{}".format(hospital_data_path, df_with_path['Patient folder'].loc[i], df_with_path["repeated path"].loc[i].split('/')[-1])
                if os.path.exists(abs_path):
                    df_with_path.loc[i, "repeated path"] = abs_path
                    bool_vector.append(i)
                else:
                    decoded_path = manipulate_string(bytes(df_with_path["repeated path"].loc[i],'utf-8').decode("ANSI"), {U"\u00AE": ".", U"\u00B2": "r", U"\u00A0": ' ', U"\u00A3": 'c', 
                                                                                                                        U"\u00AD": 'm', U"\u00A9": ')', U"\u00B3": "s", U"\u00A8": "(",
                                                                                                                        U"\u00AC": 'l', U"\u00a5": 'e', ".°": 'np', "årm": "år-", 
                                                                                                                        "/fr ": "/før ", " fr ": " før ", U"\u00a1": "A", U"\u00b4": "t",
                                                                                                                        U"\u00AA": 'j', "årl": 'år,', ".¤": 'nd', U"\u00AB": 'k',
                                                                                                                        U"\u00A2": 'b', U"\u00A7": '\'', "1岯": "/", "Ã¸": 'ø', "Ãe": 'å'})
                    if ((hospital == "Aalborg") | (hospital == "Aarhus")): 
                        abs_path = "{}\\{}\\{}".format(hospital_data_path, df_with_path['Patient folder'].loc[i], decoded_path.split('/')[-1])
                    else:
                        abs_path = "{}{}".format(hospital_data_path, decoded_path.split("BCCT HYPO {} 03.2018".format(hospital).lower().replace("/", "\\"))[-1].replace("/", "\\"))
                    if os.path.exists(abs_path):
                        df_with_path.loc[i, "repeated path"] = abs_path
                        bool_vector.append(i)
                    else:
                        if "  " in abs_path:
                            if os.path.exists(abs_path.replace("  ", " ")):
                                df_with_path.loc[i, "repeated path"] = abs_path.replace("  ", " ")
                                bool_vector.append(i)
                            elif os.path.exists(abs_path.replace("  ", "")):
                                df_with_path.loc[i, "repeated path"] = abs_path.replace("  ", "")
                                bool_vector.append(i)
                            else:
                                errors.append(abs_path)
                        elif os.path.exists(abs_path.replace(abs_path.split("\\")[-1][0:len(abs_path.split("\\")[-2])-1],  abs_path.split("\\")[-2][0:len(abs_path.split("\\")[-2])])):
                            df_with_path.loc[i, "repeated path"] = abs_path.replace(abs_path.split("\\")[-1][0:len(abs_path.split("\\")[-2])-1],  abs_path.split("\\")[-2][0:len(abs_path.split("\\")[-2])])
                            bool_vector.append(i)
                        elif os.path.exists(abs_path.replace(abs_path.split("\\")[-1][0:3],  (abs_path.split("\\")[-2][0:3] + abs_path.split("\\")[-1][4]))):
                            df_with_path.loc[i, "repeated path"] = abs_path.replace(abs_path.split("\\")[-1][0:3],  (abs_path.split("\\")[-2][0:3] + abs_path.split("\\")[-1][4]))
                            bool_vector.append(i)
                        else:
                            errors.append(abs_path)
            dfs.append(df_with_path.loc[bool_vector]) #End of admin file handling
            for path, subdirs, files in os.walk(hospital_data_path):
                for name in files:
                    if name.lower() == localxml_db_filename.lower():
                        df = pd.DataFrame()                                
                        filepath = os.path.join(path, name)
                        # print(filepath)
                        with open(filepath, "r") as file:
                            # Read each line in the file, readlines() returns a list of lines
                            content = file.readlines()
                            # Combine the lines in the list into a string
                            content = "".join(content)
                            bs_content = bs(content, "lxml")

                            for worksheet in bs_content.find_all('worksheet'):
                                rows = worksheet.find_all('row')
                                # header_row = worksheet.select('table > row')[0]
                                header_row = rows[0]
                                headers = header_row.find_all('cell')
                                #find the cell which contains the 'repeated path' tag
                                #header = header_row.find('cell', text='repeated path')
                                
                                for row_index in range(1,len(rows)):
                                    cnt += 1
                                    nipple_cnt = 0
                                    for header_index in range(len(headers)):
                                        header = headers[header_index].text.strip()
                                        dataline = rows[row_index]
                                        data = dataline.find_all('cell')
                                        txt = data[header_index].text.strip()
                                        if header == "repeated path": 
                                            folder = os.path.basename(os.path.dirname(txt))
                                        
                                            filestr = os.path.join(os.path.dirname(filepath), os.path.basename(txt))
                                            if not(os.path.exists(filestr)):
                                            #    print(filestr)
                                                corruptfilename = os.path.basename(txt)
                                                #replace the superscript 2 with a dot 
                                                corruptfilename = corruptfilename.replace(u"\u00AE", '.')
                                                #replace the superscript 2 with a r                             
                                                corruptfilename = corruptfilename.replace(u"\u00B2", 'r')
                                                #replace the non gap space with a full space
                                                corruptfilename = corruptfilename.replace(u"\u00A0", ' ')
                                                filestr = os.path.join(os.path.dirname(filepath),corruptfilename)
                                            #print(filestr, os.path.exists(filestr))
                                            if os.path.exists(filestr):
                                                df.loc[row_index, header] = filestr
                                            else:
                                                df.loc[row_index, header] = np.NaN
                                        else:
                                            if header == 'Left nipple x':
                                                nipple_cnt +=1
                                                header = "{}_{}".format(header, nipple_cnt)
                                            df.loc[row_index, header] = txt
                                    if cnt%84 == 0:
                                        percenter +=1
                                        print("{}% {}".format(percenter, "finished"))
                            collected_df.append(df)

        for i in range(len(errors)):
            print(errors[i].split("\\")[-2])
        df_administration_files = pd.concat(dfs, axis=0).rename(columns={"Left nipple x.1" : 'Right nipple x'})
        df_administration_files["repeated path"] = df_administration_files["repeated path"].str.rstrip().str.lower() 
        collected_bcct_core_data = pd.concat(collected_df).reset_index(drop=True).rename(columns={'Left nipple x_1': "Left nipple x", "Left nipple x_2" : 'Right nipple x'})
        collected_bcct_core_data["repeated path"] = collected_bcct_core_data["repeated path"].str.rstrip().str.lower() 
        suffix = "from_admin_file" 
        merged_pre_drop = collected_bcct_core_data.merge(df_administration_files, on="repeated path", suffixes=('', suffix))
        merged = merged_pre_drop.drop(columns=(collected_bcct_core_data.columns.drop(["Overall classification", "repeated path"]) + suffix)) #Add to this list if more data from admin file is required. 
        pd.to_pickle(merged, save_directory+ "\\merged_bcct_core_and_admin_files.pkl")
    else:
        merged = pd.read_pickle(save_directory+"\\merged_bcct_core_and_admin_files.pkl")

    return merged
